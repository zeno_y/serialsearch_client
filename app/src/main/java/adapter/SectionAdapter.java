package adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.echo_usa.echotech.R;

import java.util.List;
import java.util.Locale;

import custom.TextViewEmptyText;
import model.Section;
import model.SectionValue;

/**
 * Created by Zeno Yuki on 4/6/18.
 *
 * Holds multiple viewtypes to be displayed in {@link fragment.FragmentResult}. Should follow the
 * format below:
 *
 * #0: HeaderView, Registration (position guaranteed) #1: Multiple RegItemView. If none exist, will
 * display one EmptyView #2: ButtonsView #3: HeaderView, Warranty #4: Multiple LinkItemViews. If
 * none exist, will display one EmptyView #5: HeaderView, Documentation #6: Multiple LinkItemViews.
 * If none exist, will display one EmptyView
 *
 * This adapter, therefore, must manage at least seven views depending on how much content returned
 * from data source (in this case external server).
 *
 * Name of views above do not correspond to custom views, but rather are shorthands for layout
 * files: HeaderView -> {@link com.echo_usa.echotech.R.layout#item_result_header} RegItemView ->
 * {@link com.echo_usa.echotech.R.layout#item_result_static} ButtonsView -> {@link
 * com.echo_usa.echotech.R.layout#item_result_button} LinkItemView -> {@link
 * com.echo_usa.echotech.R.layout#item_result_link} EmptyView -> {@link
 * com.echo_usa.echotech.R.layout#list_empty_fixed_h}
 *
 * New sections can be added by simply adding more {@link Section} objects to the list.
 */
public class SectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final long NOT_FOUND = -1L; //Integer to return if there are issues with getting position

    //ViewTypes
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM_STATIC = 1;
    private static final int TYPE_ITEM_LINK = 2;
    private static final int TYPE_EMPTY = 3;
    private static final int TYPE_BUTTONS = 4;

    private List<Section> mList;
    private View.OnClickListener mListener;

    /**
     * Constructor.
     *
     * @param list is of type {@link List<Section>} and is the list of result items to display.
     * @param l    is of type {@link View.OnClickListener} and is the listener used for Buttons and
     *             link views
     */
    public SectionAdapter(List<Section> list, View.OnClickListener l) {
        setList(list);
        setListener(l);
    }

    /**
     * Instantiates new ViewHolders depending on {@param viewType}
     *
     * @param parent   is of type {@link ViewGroup} and is the parent view of this list/adapter
     * @param viewType is of primitive type int and is the viewtype to display
     * @return is of type {@link RecyclerView.ViewHolder} and is any one of the custom ViewHolders
     * declared in this class.
     * @see HeaderHolder
     * @see StaticHolder
     * @see LinkHolder
     * @see ButtonsHolder
     * @see EmptyHolder
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType) {
            case TYPE_HEADER:
                return new HeaderHolder(
                        (TextView)inflater.inflate(
                                R.layout.item_result_header, parent, false)
                );
            case TYPE_ITEM_STATIC:
                return new StaticHolder(
                        (TextView)inflater.inflate(
                                R.layout.item_result_static, parent, false)
                );
            case TYPE_ITEM_LINK:
                return new LinkHolder(
                        (TextView)inflater.inflate(
                                R.layout.item_result_link, parent, false)
                );
            case TYPE_BUTTONS:
                return new ButtonsHolder(
                        (ButtonBarLayout)inflater.inflate(
                                R.layout.item_result_button, parent, false)
                );
            //Default should catch Section.TYPE_EMPTY
            default:
                return new EmptyHolder(
                        (TextView)inflater.inflate(
                                R.layout.list_empty_fixed_h, parent, false)
                );
        }
    }

    /**
     * Binds the viewholder with the data at position {@param position}. Different data will bind
     * depending on the viewtype. A
     *
     * @param holder   is of type {@link RecyclerView.ViewHolder} and is any one of the custom
     *                 viewholders declared in this class.
     * @param position is of primitive type int and is the position in the list that is current
     *                 being processed.
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Section s;

        switch(holder.getItemViewType()) {
            case TYPE_HEADER:
                ((HeaderHolder)holder).setText(getViewText(position));
                break;
            case TYPE_ITEM_STATIC:
                ((StaticHolder)holder).setText(getViewText(position));
                break;
            case TYPE_ITEM_LINK:
                ((LinkHolder)holder).setTag(getSectionValueById(getItemId(position)));
                ((LinkHolder)holder).setText(getViewText(position));

                ((LinkHolder)holder).setOnClickListener(getListener());
                break;
            case TYPE_BUTTONS:
                s = getSectionByPosition(position);
                ((ButtonsHolder)holder).enableButtons(s.getItemList().size() > 0);
                ((ButtonsHolder)holder).setTags(s);

                ((ButtonsHolder)holder).setOnClickListener(getListener());
                break;
            case TYPE_EMPTY:
                s = getSectionByPosition(position);
                ((EmptyHolder)holder).setEmptyText(s.getType().getName().toLowerCase(Locale.US));
                break;
        }
    }

    /**
     * Retrieves {@link Section} that resides at adapter position {@param adapterPos}, which is
     * different from the list item position.
     *
     * @param adapterPos is of primitive type int and is the position of the item in the adapter
     *                   that is currently being processed.
     * @return is of type {@link Section} and is the item in the stored list associated to the
     * position of the adapter {@param adapterPos}
     */
    @NonNull
    private Section getSectionByPosition(int adapterPos) {
        int i = 0;
        for(Section s : getList()) {
            i++; //Accounts for section header view
            i += Math.max(s.getItemCount(), 1); //item views and one empty view if none exist
            if(s.getType().hasButtons()) i++; //buttons view if r has buttons

            if(adapterPos < i) return s;
        }

        return new Section();
    }

    /**
     * Retrieves item ID.
     *
     * @param adapterPos is of primitive type int and is the adapter position that is currently
     *                   being processed
     * @return is of primitive type long and is the ID of the item at position {@param adapterPos}
     */
    @Override
    public long getItemId(int adapterPos) {
        int i = 0;
        for(Section s : getList()) {
            i++; //Accounts for section header view
            if(adapterPos >= i && adapterPos < i + s.getItemCount()) {
                return s.getItemList().get(adapterPos - i).getId();
            }
            i += Math.max(s.getItemCount(), 1); //item views and one empty view if none exist
            if(s.getType().hasButtons()) i++; //account for buttons view if r has buttons
        }

        return -1L;
    }

    /**
     * Retrieves item count. If any of the {@link Section#getItemCount()} returns 0 (meaning the
     * list is null or the list is empty), then method takes this into consideration and adjusts
     * count by one to accommodate for the empty view that needs to be displayed
     *
     * @return is of primitive type int and is the number of items within the list (including
     * headers, result items or empty views, buttons)
     */
    @Override
    public int getItemCount() {
        if(getList() == null) return 0;

        int count = 0;
        for(Section s : getList()) {
            count++; //Accounts for header
            count += Math.max(s.getItemCount(), 1); //item views and one empty view if none exist
            if(s.getType().hasButtons()) count++; //buttons view if r has buttons
        }

        return count;
    }

    /**
     * Retrieves view type depending on item position.
     *
     * This method basically drives how each {@link Section} is going to be displayed. 1. Section
     * Header 2. Section list items or Empty View 3. Button View, if required by {@link
     * model.SectionType#hasButtons()}
     *
     * @param position is of primitive type int and is the position of the item to be displayed
     * @return By default returns {@link #TYPE_EMPTY} and said code will execute unless prior
     * conditions are met.
     */
    @Override
    public int getItemViewType(int position) {
        int i = 0;
        for(Section s : getList()) {
            if(position == i) return TYPE_HEADER;
            i++; //Accounts for header position

            //Minimum item count is always 1 since if there are no items, then one "empty view" will show
            final int minItemCount = Math.max(s.getItemCount(), 1);
            if(position >= i && position < i + minItemCount) {
                if(s.getItemCount() == 0) return TYPE_EMPTY;

                final int listPos = position - i;
                final int itemType = s.getItemList().get(listPos).getType();
                return translateItemType(itemType);
            }
            i += minItemCount; //item views and one empty view if none exist

            if(s.getType().hasButtons()) {
                if(position == i) return TYPE_BUTTONS;
                i++; //buttons view if r has buttons
            }
        }

        Log.i(getClass().getSimpleName(), "shouldn't reach this");
        return TYPE_EMPTY;
    }

    /**
     * Replaces the section within this adapter list with a section with the same type
     *
     * @param section is of type {@link Section} and is the section object to replace one in the
     *                list.
     */
    public void replace(Section section) {
        for(int i = 0; i < getList().size(); i++) {
            Section sectionFromList = getList().get(i);

            if(sectionFromList.getType().getId() == section.getType().getId()) {
                getList().set(i, section);
                notifyDataSetChanged();
                return; //breaks loop when section is replaced.
            }
        }
    }

    /**
     * Retrieves String resource for recycler's views.
     *
     * @param adapterPos is of primitive type int and is the position in the recyclerview that is
     *                   currently being processed.
     * @return is of primitive type int and is the string resource ID of the header
     */
    @Nullable
    private String getViewText(int adapterPos) {
        int i = 0;
        for(Section r : getList()) {
            if(adapterPos == i) return r.getType().getName();
            i++; //Accounts for header position

            if(adapterPos >= i && adapterPos < i + r.getItemCount()) {
                return r.getItemList().get(adapterPos - i).getValue();
            }

            i += Math.max(r.getItemCount(), 1); //item views and one empty view if none exist
            if(r.getType().hasButtons()) i++; //buttons view if r has buttons
        }

        return null;
    }

    /**
     * Retrieves the item from the list that has an ID {@param id}.
     *
     * @param id is of primitive type long and is the ID of the item to return.
     * @return is of type {@link SectionValue}.
     */
    @Nullable
    private SectionValue getSectionValueById(long id) {
        if(getList() == null) return null;

        for(Section r : getList()) {
            for(SectionValue item : r.getItemList()) {
                if(item.getId() == id) return item;
            }
        }

        return null;
    }

    /**
     * Converts {@link SectionValue#getType()} to a view type, declared in this class.
     *
     * @param itemType is of primitive type int and is {@link SectionValue#TYPE_STATIC} or {@link
     *                 SectionValue#TYPE_LINK}
     * @return is of primitive type int and is a view type, dependent on {@link
     * SectionValue#getType()}
     */
    private int translateItemType(int itemType) {
        if(itemType == SectionValue.TYPE_STATIC) return TYPE_ITEM_STATIC;
        else if(itemType == SectionValue.TYPE_LINK) return TYPE_ITEM_LINK;
        else return TYPE_EMPTY;
    }

//    @StringRes
//    private int getEmptyTextRes(@Nullable String sectionType) {
//        sectionType = sectionType == null ? "thing" : sectionType;
//
//
//        switch(sectionType) {
//            case "Registration": return R.string.text_no_registration;
//            case "Warranty": return R.string.text_no_warranty;
//            case "Documents": return R.string.text_no_documents;
//            default: return R.string.text_no_show_default;
//        }
//    }

    /**
     * Retrieves {@link List<Section>} driving this adapter.
     *
     * @return is of type {@link List<Section>}
     */
    private List<Section> getList() {return mList;}

    /**
     * Sets the list that will drive this adapter.
     *
     * @param list is of type {@link List<Section>}
     */
    private void setList(List<Section> list) {mList = list;}

    /**
     * Retrieves view listener {@link View.OnClickListener} that was passed from the class that
     * instantiated this adapter.
     *
     * @return is of type {@link View.OnClickListener}
     */
    private View.OnClickListener getListener() {return mListener;}

    /**
     * Sets view listener {@link View.OnClickListener} to class variable
     *
     * @param l is of type {@link View.OnClickListener} and is the listener passed from the class
     *          that instantiated this adapter.
     */
    private void setListener(View.OnClickListener l) {mListener = l;}

    /**
     * Holds header views for Recyclerview to recycle. Extends {@link RecyclerView.ViewHolder}. Has
     * shadow methods to pass data to the view that is being held.
     */
    static class HeaderHolder extends RecyclerView.ViewHolder {
        /**
         * Constructor.
         *
         * @param itemView is of type {@link TextView} and is the root view of {@link
         *                 R.layout#item_result_header}
         */
        HeaderHolder(TextView itemView) {super(itemView);}

        /**
         * Tries casting {@link #itemView} (view held by {@link RecyclerView.ViewHolder}) before
         * shadowing method {@link TextView#setText(CharSequence)}
         *
         * @param headerId is of primitive type int and is the string res to set the view to
         */
        void setText(CharSequence headerId) {
            try {
                ((TextView)itemView).setText(headerId);
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Holds static item views for Recyclerview to recycle. Extends {@link RecyclerView.ViewHolder}.
     * Has shadow methods to pass data to the view that is being held.
     */
    static class StaticHolder extends RecyclerView.ViewHolder {
        /**
         * Constructor.
         *
         * @param itemView is of type {@link TextView} and is the root view of {@link
         *                 R.layout#item_result_static}
         */
        StaticHolder(TextView itemView) {super(itemView);}

        /**
         * Tries casting {@link #itemView} (view held by {@link RecyclerView.ViewHolder}) before
         * shadowing method {@link TextView#setText(CharSequence)}
         *
         * @param c is of type {@link CharSequence} and is the text to set the held view to
         */
        void setText(CharSequence c) {
            try {
                ((TextView)itemView).setText(c);
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Holds link item views for Recyclerview to recycle. Extends {@link RecyclerView.ViewHolder}.
     * Has shadow methods to pass data to the view that is being held.
     */
    static class LinkHolder extends RecyclerView.ViewHolder {
        LinkHolder(TextView itemView) {super(itemView);}

        /**
         * Tries casting {@link #itemView} (view held by {@link RecyclerView.ViewHolder}) before
         * shadowing method {@link TextView#setText(CharSequence)}
         *
         * @param c is of type {@link CharSequence} and is the text to set the held view to
         */
        void setText(CharSequence c) {
            try {
                ((TextView)itemView).setText(c);
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }

        /**
         * Sets click listener to view.
         *
         * @param listener is of type {@link View.OnClickListener} and is the listener passed
         *                 through the constructor
         */
        void setOnClickListener(View.OnClickListener listener) {
            itemView.setOnClickListener(listener);
        }

        /**
         * Shadows method {@link View#setTag(Object)}. Stores view's associated data to be retrieved
         * at a later point in time from another class.
         *
         * @param item is of type {@link SectionValue} and is this view's associated data.
         */
        void setTag(SectionValue item) {if(itemView != null) itemView.setTag(item);}
    }

    /**
     * Holds Buttons view for Recyclerview to recycle. Extends {@link RecyclerView.ViewHolder}. Has
     * shadow methods to pass data to the view that is being held.
     */
    static class ButtonsHolder extends RecyclerView.ViewHolder {
        Button mButtonLeft, mButtonRight;

        /**
         * Constructor.
         *
         * @param itemView is of type {@link ButtonBarLayout} and is the root view of {@link
         *                 R.layout#item_result_button}
         */
        ButtonsHolder(ButtonBarLayout itemView) {
            super(itemView);

            mButtonLeft = itemView.findViewById(R.id.button_left);
            mButtonRight = itemView.findViewById(R.id.button_right);
        }

        /**
         * Checks for null on {@link #mButtonLeft} before shadowing method {@link
         * Button#setText(int)}
         *
         * @param stringRes is of primitive type int and is the ID of the string res to set to the
         *                  view
         */
        void setLeftBtnText(@StringRes int stringRes) {
            if(mButtonLeft != null) mButtonLeft.setText(stringRes);
        }

        /**
         * Checks for null on {@link #mButtonRight} before shadowing method {@link
         * Button#setText(int)}
         *
         * @param stringRes is of primitive type int and is the ID of the string res to set to the
         *                  view
         */
        void setRightBtnText(@StringRes int stringRes) {
            if(mButtonRight != null) mButtonRight.setText(stringRes);
        }

        /**
         * Checks for null on {@link #mButtonLeft} before shadowing method {@link
         * Button#setEnabled(boolean)}
         *
         * @param enable is of primitive type boolean and is true when button needs enabling.
         */
        void enableBtnLeft(boolean enable) {
            if(mButtonLeft != null) mButtonLeft.setEnabled(enable);
        }

        /**
         * Checks for null on {@link #mButtonRight} before shadowing method {@link
         * Button#setEnabled(boolean)}
         *
         * @param enable is of primitive type boolean and is true when button needs enabling.
         */
        void enableBtnRight(boolean enable) {
            if(mButtonRight != null) mButtonRight.setEnabled(enable);
        }

        /**
         * Shadows method {@link View#setTag(Object)}. Stores view's associated data to be retrieved
         * at a later point in time from another class.
         *
         * @param s is of type {@link Section} and is this view's associated data.
         */
        void setTags(@Nullable Section s) {
            mButtonLeft.setTag(s);
            mButtonRight.setTag(s);
        }

        /**
         * Enables buttons accordingly. Note that one button will be disabled while the other is
         * enabled.
         *
         * @param hasButtons is of primitive type boolean and is true if the current {@link Section}
         *                   being processed has content.
         */
        void enableButtons(boolean hasButtons) {
            mButtonLeft.setEnabled(hasButtons);
            mButtonRight.setEnabled(!hasButtons);
        }

        /**
         * Sets click listener to buttons.
         *
         * @param listener is of type {@link View.OnClickListener} and is the listener passed
         *                 through the constructor
         */
        void setOnClickListener(View.OnClickListener listener) {
            mButtonLeft.setOnClickListener(listener);
            mButtonRight.setOnClickListener(listener);
        }
    }

    /**
     * Holds Empty view for Recyclerview to recycle. Extends {@link RecyclerView.ViewHolder}. This
     * holder does nothing special.
     */
    static class EmptyHolder extends RecyclerView.ViewHolder {
        /**
         * Constructor.
         *
         * @param itemView is of type {@link TextView} and is the root view of {@link
         *                 R.layout#list_empty_fixed_h}
         */
        EmptyHolder(TextView itemView) {super(itemView);}

        /**
         * Shadows method {@link TextViewEmptyText#setEmptyText(CharSequence)} but only after
         * checking for null and making sure {@link #itemView} is an instance of {@link
         * TextViewEmptyText}
         *
         * @param sectionName is of type {@link CharSequence} and is the section name
         */
        void setEmptyText(@Nullable CharSequence sectionName) {
            if(itemView == null) return;

            try {
                ((TextViewEmptyText)itemView).setEmptyText(sectionName);
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }
    }
}
