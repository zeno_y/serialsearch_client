package adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

import custom.HistoryItemView;
import model.HistoryItem;

/**
 * Extends {@link RecyclerView.Adapter}. Handles {@link HistoryItem} data for a lists that uses
 * {@link HistoryItemView} to display information.
 *
 * Uses an extension of {@link RecyclerView.ViewHolder} to hold onto references to newly
 * instantiated views. When user scrolls through list, views are "recycled" using data that is now
 * visible due to the scrolling.
 *
 * Constructor receives a listener that this adapter attaches to each view/ViewHolder since
 * RecyclerView doesn't natively support an "onItemClickListener".
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {
    private static final long NOT_FOUND = -1L; //Integer to return if there are issues with getting position

    private List<HistoryItem> mList;
    private View.OnClickListener mListener; //Stores reference to
    @LayoutRes
    private int mRes; //Stores layout resource ID of item view, passed through constructor

    /**
     * Constructor. Parameters are stored for use elsewhere.
     *
     * @param list is of type {@link List<HistoryItem>} and is a list of data to display.
     * @param l    is of type {@link View.OnClickListener} and is the listener to attach to every
     *             view.
     * @param res  is of primitive type int and is the layout resource of the item view.
     */
    public HistoryAdapter(List<HistoryItem> list, View.OnClickListener l, @LayoutRes int res) {
        setList(list);
        setListener(l);
        setRes(res);
    }

    /**
     * Fires when the instantiation of a new view is required. Binds the new view to a returned
     * ViewHolder.
     *
     * @param parent   is of type {@link ViewGroup} and is the parent of the listview that this
     *                 adapter is attached to.
     * @param viewType is of primtive type int and is the viewtype returned from {@link
     *                 #getItemViewType(int)}
     * @return is of type {@link HistoryHolder} and holds as reference to {@link HistoryItemView}
     */
    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryHolder(
                LayoutInflater.from(parent.getContext()).inflate(getRes(), parent, false)
        );
    }

    /**
     * Fires when the RecyclerView determines the view at position {@param position} requires
     * updating (be it because it's displaying the view for the first time or because the user has
     * scrolled the list).
     *
     * @param holder   is of type {@link HistoryHolder} and is the holder that holds the view to
     *                 display.
     * @param position is of primtive type int and is the position of the view in the adapter/list.
     */
    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        HistoryItem item = getItemAtPosition(position);

        holder.setTag(item);
        holder.setTexts(item.getModel(), item.getSerial(), item.getSearchDate());

        holder.setListener(getListener());
    }

    /**
     * Retrieves item within list {@link #mList}.
     *
     * @param position is of primitive type int and is the list position to retrieve the item from
     * @return is of type {@link HistoryItem} and is the list item at position {@param position}
     */
    @Nullable
    public HistoryItem getItem(int position) {return mList.get(position);}

    /**
     * Nullable. Retrieves item from {@link #mList} by ID. If ID isn't found, returns null.
     *
     * @param id is of primitive type long and is the ID of a {@link HistoryItem} in {@link #mList}.
     *           May equal {@link #NOT_FOUND}.
     * @return is of type {@link HistoryItem} and is the item in {@link #mList} with ID {@param id}
     */
    @Nullable
    private HistoryItem getItemById(long id) {
        for(HistoryItem item : mList) {
            if(item.getId() == id) return item;
        }

        return null;
    }

    /**
     * Retrieves list size
     *
     * @return is of primitive type int and is the size of the item list {@link #mList}
     */
    @Override
    public int getItemCount() {return getList().size();}

    /**
     * Determines if list is empty or null.
     *
     * @return is of primitive type boolean and is true if list is empty or null.
     */
    public boolean isEmpty() {return getList() == null || getList().isEmpty();}

    /**
     * Clears list for this adapter. Database data is handled elsewhere.
     */
    public void clearList() {
        mList.clear();
    }

    /**
     * Retrieves list item ID.
     *
     * @param position is of primitive type int and is the list position to retrieve the ID from
     * @return is of primitive type long and is the ID of the item in list position {@param
     * position}
     */
    @Override
    public long getItemId(int position) {
        if(mList == null) return NOT_FOUND;
        return mList.get(position).getId();
    }

    /**
     * Adds {@param item} to the beginning of the list.
     *
     * @param item is of type {@link HistoryItem} and holds the newly searched item.
     */
    public void addAsFirst(@NonNull HistoryItem item) {mList.add(0, item);}

    /**
     * Updates item with data passed through as parameters. Moves updated item to the top of the
     * list. Requires a call to {@link #notifyDataSetChanged()} to update UI.
     *
     * @param id   is of primitive type long and is the ID of the item in the list to update
     * @param date is of primitive type long and is the new date of the item with ID {@param id}
     */
    public void updateItem(long id, long date) {
        HistoryItem item = getItemById(id);

        if(item == null) return;
        mList.remove(item); //Removes item from list prior to be added back in later

        item.setSearchDate(date); //Updates search date of item
        addAsFirst(item);
    }

    /**
     * Retrieves {@link HistoryItem} by adapter position.
     *
     * @param position is of primitive type int and is the position of the item to retrieve.
     * @return is of type {@link HistoryItem} and is the item at position {@param position}
     */
    private HistoryItem getItemAtPosition(int position) {return getList().get(position);}

    //Setters & Getters
    private void setList(List<HistoryItem> list) {mList = list;}
    private List<HistoryItem> getList() {return mList;}

    private void setListener(View.OnClickListener l) {mListener = l;}
    private View.OnClickListener getListener() {return mListener;}

    private void setRes(@LayoutRes int res) {mRes = res;}
    @LayoutRes private int getRes() {return mRes;}

    /**
     * Extends {@link RecyclerView.ViewHolder} Custom implementation that stores a reference to a
     * list view while shadowing methods to said view.
     */
    static class HistoryHolder extends RecyclerView.ViewHolder {
        /**
         * Constructor. Stores reference to parameter view by calling super.
         *
         * @param itemView is of type {@link View} and is the view to hold a reference to.
         */
        HistoryHolder(View itemView) {super(itemView);}

        /**
         * Shadows method {@link View#setTag(Object)}. Stores view's associated data to be retrieved
         * at a later point in time from another class.
         *
         * @param item is of type {@link HistoryItem} and is this view's associated data.
         */
        void setTag(HistoryItem item) {if(itemView != null) itemView.setTag(item);}

        /**
         * Shadows method {@link HistoryItemView#setTexts(CharSequence, CharSequence, CharSequence)}
         * but only after checking for null and making sure {@link #itemView} is an instance of
         * {@link HistoryItemView}.
         *
         * @param model  is of type {@link CharSequence} and is the model name.
         * @param serial is of type {@link CharSequence} and is the serial number of the model.
         * @param date   is of primitive type long and is the date in which the search was
         *               conducted.
         */
        void setTexts(CharSequence model, CharSequence serial, long date) {
            if(itemView == null) return;

            //Converts long date into human friendly format.
            final CharSequence dateSequence = DateFormat
                    .getDateInstance(DateFormat.SHORT, Locale.US).format(date);

            //Tries to set text.
            try {
                ((HistoryItemView)itemView).setTexts(model, serial, dateSequence);
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }

        /**
         * Shadows method {@link View#setOnClickListener(View.OnClickListener)}
         *
         * @param l is of type {@link View.OnClickListener} and is the listener to attach to the
         *          referenced view (ie {@link #itemView})
         */
        void setListener(View.OnClickListener l) {
            if(itemView != null) itemView.setOnClickListener(l);
        }
    }
}
