package volley;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.echo_usa.echotech.SerialSearchActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.Section;
import model.SectionType;
import model.SectionValue;

/**
 * Created by Zeno Yuki on 4/8/18.
 *
 * Implements {@link VolleyConst} to access constant values. All constants seen here come from said
 * implemented class.
 *
 * Communicates with remote through Volley in regards to serial number data.
 */
public class VolleySerial implements VolleyConst {
    /**
     * Requests data regarding serial numbers from remote through Volley.
     *
     * @param isNew is final and of primitive type boolean
     * @param input is final, non-null and of type {@link String}
     * @param cb    is final and of type {@link Callback}
     * @return
     */
    public static Request request(final boolean isNew, @NonNull final String input, final Callback cb) {
        return new JsonObjectRequest(URI_GET_SERIAL + input, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<Section> sectionsList = new ArrayList<>();

                try {
                    final String serial = response.getString(SERIAL);
                    final String model = response.getString(MODEL);

                    JSONArray sectionArray = response.getJSONArray(SECTIONS_ARRAY);
                    for(int i = 0; i < sectionArray.length(); i++) {
                        final long sectionTypeId = sectionArray.getJSONObject(i).getLong(SECTION_TYPE_ID);

                        Section s = new Section();
                        s.setSerialNum(serial);
                        s.setModel(model);
                        s.setType(cb.getSectionType(sectionTypeId));

                        JSONArray valuesArray = sectionArray.getJSONObject(i).getJSONArray(VALUES_ARRAY);
                        for(int j = 0; j < valuesArray.length(); j++) {
                            final JSONObject obj = valuesArray.getJSONObject(j);
                            SectionValue sv = new SectionValue();

                            sv.setId(obj.getLong(ID));
                            sv.setType(obj.getInt(TYPE));
                            sv.setValue(obj.getString(VALUE));

                            s.addItem(sv);
                        }

                        sectionsList.add(s);
                    }
                    cb.onResponseSuccess(isNew, sectionsList);
                } catch(JSONException e) {
                    cb.onResponseFail(e.getMessage(), SerialSearchActivity.ACTION_NONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message;
                int action;

                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null) {
                    message = trimMsg(new String(response.data), "message");
                    action = SerialSearchActivity.ACTION_NONE;

                    switch(response.statusCode) {
                        //TODO: figure out some cases here
                    }
                } else {
                    message = "Search came back empty.";
                    action = SerialSearchActivity.ACTION_SN_HELP;
                }

                if(message != null) cb.onResponseFail(message, action);
            }
        });
    }

    /**
     * Trims JSON error message by retrieving data corresponding to the key {@param key}
     *
     * @param json is of type {@link String} and is the JSON data to parse through
     * @param key  is of type {@link String} and is the key value to search for.
     * @return is of type {@link String}
     */
    private static String trimMsg(String json, String key) {
        String trimmed;

        try {
            JSONObject obj = new JSONObject(json);
            trimmed = obj.getString(key);
        } catch(JSONException e) {
            e.printStackTrace();
            return null;
        }

        return trimmed;
    }

    /**
     * Callback interface to send data back to an Activity for processing.
     */
    public interface Callback {
        /**
         * Retrieves {@link SectionType} from client database by ID retrieved from remote.
         *
         * @param id is of primitive type long and is the ID of the Section Type to retrieve.
         * @return is of type {@link SectionType}
         */
        SectionType getSectionType(long id);

        /**
         * Fires when response is successful to store data into the client database.
         *
         * @param newSearch   is of primitive type boolean and is true if the performed search was a
         *                    new one.
         * @param sectionList is non-null and of type {@link List<Section>} and is the list of
         *                    sections retrieved.
         */
        void onResponseSuccess(boolean newSearch, @NonNull List<Section> sectionList);

        /**
         * Fires when response is unsuccessful.
         *
         * @param message        is of type {@link String} and is the error message to display to
         *                       the user.
         * @param snackbarAction is of primitive type int and is the Snackbar action associated to
         *                       this error message
         * @see SerialSearchActivity#ACTION_NONE
         * @see SerialSearchActivity#ACTION_SN_HELP
         */
        void onResponseFail(String message, int snackbarAction);
    }
}
