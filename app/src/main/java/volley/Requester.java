package volley;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Zeno Yuki on 6/12/18.
 *
 * Requester class that acts as a singleton.
 */
public class Requester {
    private static final int TIMEOUT_IN_MS = 7000; //Timeout in milliseconds for all requests.
    private static Requester sThisInstance;
    private RequestQueue mRequestQueue;

    /**
     * Private contstructor to prevent other classes from instantiating.
     *
     * @param c is of type {@link Context} and is the context of the calling class.
     */
    private Requester(Context c) {mRequestQueue = Volley.newRequestQueue(c);}

    /**
     * Retrieves class's single instance.
     *
     * @param c is of type {@link Context} and is the context of the calling class.
     * @return is of type {@link Requester} and is the only instantiated instance of this class.
     */
    public static Requester getInstance(Context c) {
        if(sThisInstance == null) sThisInstance = new Requester(c);
        return sThisInstance;
    }

    /**
     * Adds request {@link VolleySerial#request(boolean, String, VolleySerial.Callback)} to the
     * request queue
     *
     * @param newSearch    is of primitive type boolean and is true when search is brand new (typed
     *                     in).
     * @param serialNumber is of type {@link String} and is the serial number that was searched.
     * @param cb           is of type {@link VolleySerial.Callback} and is the class that implements
     *                     said interface.
     */
    public void requestSn(boolean newSearch, String serialNumber, VolleySerial.Callback cb) {
        addToRequestQueue(VolleySerial.request(newSearch, serialNumber, cb));
    }

    /**
     * Adds request {@link VolleySectionType#request(VolleySectionType.Callback)} to the request
     * queue.
     *
     * @param cb is of type {@link VolleySectionType.Callback} and is the class that implements said
     *           interface.
     */
    public void requestSectionTypes(VolleySectionType.Callback cb) {
        addToRequestQueue(VolleySectionType.request(cb));
    }

    /**
     * Adds Volley requestTypes to queue {@link #mRequestQueue}
     *
     * @param req is of type {@link Request <T>} and is the requestTypes to add to the queue.
     */
    private <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_IN_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        req.setTag(getClass().getSimpleName());
        getRequestQueue().add(req);
    }

    /**
     * Retrieves Volley request queue.
     *
     * @return is of type {@link RequestQueue} and is the Volley request queue.
     */
    private RequestQueue getRequestQueue() {return mRequestQueue;}
}
