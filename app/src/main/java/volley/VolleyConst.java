package volley;

/**
 * Created by Zeno Yuki on 7/16/18. Stores commonly used constants. Implement this interface for
 * every Volley class created.
 */
interface VolleyConst {
    String CMD_GET_SERIAL = "serial?serial=";
    String CMD_GET_TYPE = "type?id=";
    String CMD_GET_ALL_SERIAL = "all-serial";
    String CMD_GET_ALL_TYPES = "all-types";
    String ROOT = "https://apps.zenoyuki.com/EchoRest/";

    String URI_GET_SERIAL = ROOT + CMD_GET_SERIAL;
    String URI_GET_ALL_SERIAL = ROOT + CMD_GET_ALL_SERIAL;
    String URI_GET_ALL_TYPES = ROOT + CMD_GET_ALL_TYPES;

    String SERIAL = "serialNum";
    String MODEL = "modelName";
    String SECTIONS_ARRAY = "sections";

    String SECTION_TYPE_ID = "typeId";
    String NAME = "name";
    String BUTTONS = "hasButtons";
    String ORDER = "order";

    String VALUES_ARRAY = "values";
    String ID = "id";
    String TYPE = "type";
    String VALUE = "value";
}
