package com.echo_usa.echotech;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class PdfViewActivity extends AppCompatActivity {
    public static final String KEY_URL = "url_key"; //variable name and value may change

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);
//
        //Sets up Actionbar by setting up HomeAsUp button and removing title text
        final ActionBar a = getSupportActionBar();
        if(a != null) {
            a.setCustomView(R.layout.toolbar_logo);
            a.setDisplayShowCustomEnabled(true);

            a.setDisplayHomeAsUpEnabled(true);
            a.setHomeAsUpIndicator(R.drawable.ic_vector_back);
            a.setHomeActionContentDescription(R.string.description_home_action);

            a.setTitle(null);
        }

        //Sets status bar translucency.
        ((Universal)getApplication()).setStatusBarTranslucent(PdfViewActivity.this, true);

        PDFView pdfView = (PDFView)findViewById(R.id.pdf_view);
//        pdfView.fromUri();
    }
}
