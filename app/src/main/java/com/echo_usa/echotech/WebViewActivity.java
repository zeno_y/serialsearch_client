package com.echo_usa.echotech;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class WebViewActivity extends AppCompatActivity {
    public static final String KEY_URL = "url_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
//
        //Sets up Actionbar by setting up HomeAsUp button and removing title text
        final ActionBar a = getSupportActionBar();
        if(a != null) {
            a.setCustomView(R.layout.toolbar_logo);
            a.setDisplayShowCustomEnabled(true);

            a.setDisplayHomeAsUpEnabled(true);
            a.setHomeAsUpIndicator(R.drawable.ic_vector_back);
            a.setHomeActionContentDescription(R.string.description_home_action);

            a.setTitle(null);
        }

        //Sets status bar translucency.
        ((Universal)getApplication()).setStatusBarTranslucent(WebViewActivity.this, true);

        final ProgressBar progress = (ProgressBar)findViewById(R.id.web_progress);

        //Configures WebView
        //TODO: errors are occuring left and right. not configured correctly. Must adjust
        WebView webView = (WebView)findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progress.setProgress(newProgress);
                if(newProgress == 100) progress.setVisibility(View.GONE);
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });

        webView.loadUrl("https://portal.echo-usa.com");
//        webView.loadUrl("http://storage.zenoyuki.com/echotech/mockdoc.pdf");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_transition_parent_in, R.anim.activity_transition_child_out);
    }
}
