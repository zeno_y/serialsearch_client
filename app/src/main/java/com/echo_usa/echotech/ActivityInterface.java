package com.echo_usa.echotech;

/**
 * Created by Zeno Yuki on 4/18/18
 *
 * Shadows {@link android.support.v7.app.AppCompatActivity#onWindowFocusChanged(boolean)}
 * Notifies implemented class of when Activity's focus changes
 */
public interface ActivityInterface {
    /**
     * Notifies implemented class when Activity's focus changes
     *
     * @param activityHasFocus True when Activity changed to have focus
     */
    void onActivityWindowFocusChanged(boolean activityHasFocus);
}
