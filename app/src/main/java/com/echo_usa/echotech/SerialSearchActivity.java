package com.echo_usa.echotech;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.List;

import custom.Background;
import fragment.FragmentDrawerStart;
import fragment.FragmentResult;
import fragment.FragmentSearch;
import fragment.UserInput;
import fragment.dialog.DialogClearHistory;
import fragment.dialog.DialogDetails;
import fragment.dialog.DialogRegister;
import fragment.dialog.DialogSnHelp;
import fragment.dialog.DialogWrapper;
import model.HistoryItem;
import model.Section;
import model.SectionType;
import volley.Requester;
import volley.VolleySerial;

//import android.util.Log;

/**
 * Created by Zeno Yuki on 3/21/18.
 *
 * Activity that handles user input for serial number search and displaying results.
 *
 * This activity currently handles fragment transactions for {@link FragmentSearch} and {@link
 * FragmentResult}. Transactions are closely coupled with {@link Background} animations.
 * Transactions and popbacks occur once Background animations are finished, to give the application
 * a more polished look.
 *
 * Sequence of events for searching: 1. User types/scans serial number and hits "search" (these
 * actions will occur in {@link FragmentSearch} 2. App will wait for server response (not yet
 * implemented) 3. Fires {@link #beginTransitionToResults()}, which subsequently fires {@link
 * #setBgState(int)} and removes fragment from view 4. Once Background animation ends at {@link
 * Background#STATE_BANNER} and fires {@link #onStateChangeEnded(int)}, fragment transaction will
 * begin, placing (replacing) {@link FragmentResult} into fragment container. This concludes the
 * flow.
 *
 * Sequence of events on pressing back (either system UI back or HomeAsUp back) 1. Regardless of
 * which back button the user presses, the method {@link #onHomeBackPressed()} fires. Note that this
 * method is dependent on whether {@link Background} has finished its animation. 2. {@link
 * #onHomeBackPressed()} pops a backstack (which should remove {@link FragmentResult}) and begins
 * {@link Background} animation to {@link Background#STATE_SCREEN} 3. Once animation ends, {@link
 * #onStateChangeEnded(int)} fires which pops back stack (which should) place {@link FragmentSearch}
 * back into the fragment container). This concludes the flow.
 *
 * While all of this is going on, HomeAsUp button's drawable is evaluated every time the backstack
 * changed. If needed, the drawable will change accordingly.
 */
//TODO: add activity for logging in.
//TODO: add activity for QR Scanning.
//TODO: need an about page with details on how the software works.
//TODO: search metadata will be deleted after 30 days. Search contents will NEVER be stored.
public class SerialSearchActivity extends AppCompatActivity
        implements FragmentManager.OnBackStackChangedListener,
        Background.StateCallback, VolleySerial.Callback {
    //Constants for Snackbar action
    public static final int ACTION_NONE = 0;
    public static final int ACTION_SN_HELP = 1;

    //Constants for backstack tags
    private static final String TAG_DIALOG = "dialog_tag";
    private static final String TAG_TRANSIT_TO_BLANK = "transit_to_blank";
    private static final String TAG_TRANSIT_TO_RESULT = "transit_to_result";

    private View mContent;
    private DrawerLayout mDrawer;
    private Background mBg;
    private ContentLoadingProgressBar mProgress;

    private List<Section> mSectionList;

    private ActivityInterface mActivityInterface;

    private UserInput mUserInput;

    //Variable that keeps track of which drawable is being shown as Actionbar's HomeAsUp
    @DrawableRes
    private int mHomeIndicatorRes;

    /**
     * Fires when Activity is created within the Activity lifecycle. Sets up most views for proper
     * functionality.
     *
     * @param savedInstanceState is of type {@link Bundle} and is the previously saved instance
     *                           state of this Activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial_search);

        mContent = findViewById(android.R.id.content);

        //Locks drawer to prevent user swiping right to open
        mDrawer = (DrawerLayout)findViewById(R.id.layout_drawer);
        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawer.addDrawerListener(getDrawerStart());

        //Sets callback of Background to this class
        mBg = (Background)findViewById(R.id.background);
        mBg.setCallback(SerialSearchActivity.this);

        //Sets Toolbar as ActionBar
        final Toolbar t = findViewById(R.id.toolbar_serial_search);
        setSupportActionBar(t);

        setHomeAsUpRes(R.drawable.vector_anim_menu);
//        mHomeIndicatorRes = R.drawable.vector_anim_menu;

        //Sets up Actionbar by setting up HomeAsUp button and removing title text
        final ActionBar a = getSupportActionBar();
        if(a != null) {
            a.setDisplayHomeAsUpEnabled(true);
            a.setHomeAsUpIndicator(R.drawable.ic_vector_menu);
            a.setHomeActionContentDescription(R.string.description_home_action);

            a.setTitle(null);
        }

        //Sets status bar translucency.
        ((Universal)getApplication()).setStatusBarTranslucent(SerialSearchActivity.this, true);

        //Puts the first fragment into the fragment container. Prevents transaction from being
        //placed into the backstack since user should not be allowed to revert transition.
        Fragment f = FragmentSearch.newInstance();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, f)
                .disallowAddToBackStack()
                .commit();
    }

    /**
     * Overrides super to add backstackChange listener to Fragment Manager.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("listeners", "backstack listener added at SSActivity.onResume()");
        getSupportFragmentManager().addOnBackStackChangedListener(SerialSearchActivity.this);
    }

    /**
     * Overrides super to remove backstackChange listener from Fragment Manager.
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d("listeners", "backstack listener removed at SSActivity.onPause()");
        getSupportFragmentManager().removeOnBackStackChangedListener(SerialSearchActivity.this);
    }

    /**
     * Implemented from {@link FragmentManager.OnBackStackChangedListener}. Sets {@link #mUserInput}
     * to the currently active fragment if fragment is an instance of {@link UserInput} so that
     * calls to {@link UserInput#reset()} are sent to the active fragment.
     */
    @Override
    public void onBackStackChanged() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(f instanceof UserInput) mUserInput = (UserInput)f;
        else mUserInput = null;

        Log.d("user input", mUserInput != null ? mUserInput.toString() : "null");
    }

    /**
     * Shadows method {@link Background#setContentState(int)}.
     */
    public void setBgState(int state) {mBg.setContentState(state);}

    /**
     * Clears database of search history and notifies user how many entries were deleted.
     */
    public void clearHistoryDb() {
        final int count = ((Universal)getApplication()).clearHistory();

        if(count > 0) showSnackbar(getString(R.string.snackbar_deletion, count), ACTION_NONE);
        else showSnackbar(getString(R.string.snackbar_no_del), ACTION_NONE);
    }

    /**
     * Updates and retrieves history item's date.
     *
     * @param historyItemId is of primitive type long and is the ID of the item to update.
     * @return is of primitive type long and is the new date.
     */
    public long updateAndGetDate(long historyItemId) {
        final long date = System.currentTimeMillis();
        final int count = ((Universal)getApplication()).updateSearchHistory(historyItemId, date);

        return date;
    }

    /**
     * Clears data from adapter, and subsequently the UI.
     */
    public void clearHistoryDrawerAdapter() {getDrawerStart().clearListAdapter();}

    /**
     * Determines whether the menu should be prepared for creation through the
     * onCreateOptionsMenu() method. Must return true for menu to show.
     *
     * @param menu is of type {@link Menu} and is the menu object to display
     * @return is of primitive type boolean and is true if the menu should be created.
     * @see #onCreateOptionsMenu(Menu)
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    /**
     *
     * @param menu
     * @return is of primitive type boolean and is true if the action was consumed here.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.serial_search, menu);
        return true;
    }

    /**
     * Fires when a menu button in Actionbar is pressed.
     *
     * @param item is of type {@link MenuItem} and is the menu item that was pressed
     * @return is of primitive type boolean and is true if the action was consumed within the
     * implementation of this view.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int backstackCount = getSupportFragmentManager().getBackStackEntryCount();

        switch(item.getItemId()) {
            //"hamburger" or "back" button, depending on the situation.
            case android.R.id.home:
                if(mBg.isAnimRunning()) return true;

                if(backstackCount == 0) {
                    if(mContent != null) closeKeyboard(mContent.getWindowToken());
                    mDrawer.openDrawer(Gravity.START);

                    return true;
                } else {
                    onBackPressed();
                    return true;
                }
            //Top right detail button
            case R.id.search_menu_details:
                if(mContent != null) closeKeyboard(mContent.getWindowToken());
                startDetailActivity();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Fires when system UI back is pressed.
     */
    @Override
    public void onBackPressed() {
        //Prevents anything from happening if keyboard is shown. Closes it if open
        if(mContent != null && closeKeyboard(mContent.getWindowToken())) return;

        //Prevents anything from happening if Background animation is occurring
        if(mBg.isAnimRunning()) return;

        //Closes drawer if open on first back press
        if(closeDrawer()) return;
        //Begins flow described in class header
        if(onHomeBackPressed()) return;

        //If none above are true, pass the call to super to handle the rest.
        super.onBackPressed();
    }

    /**
     * Closes {@link DrawerLayout} drawer if open.
     *
     * @return is of primitive type boolean and is true if the drawer was indeed closed.
     */
    public boolean closeDrawer() {
        if(mDrawer == null) return false;

        if(mDrawer.isDrawerOpen(Gravity.START)) {
            mDrawer.closeDrawer(Gravity.START);
            return true;
        }

        return false;
    }

    /**
     * Fires when the app believes the user wants to close search results (described in class
     * javadocs).
     *
     * @return is of primitive type boolean and is true if the "revert" flow has begun.
     */
    private boolean onHomeBackPressed() {
        final int backstackCount = getSupportFragmentManager().getBackStackEntryCount();

        if(backstackCount > 0) {
            //Currently, backstack should contain two transactions at this point
            getSupportFragmentManager().popBackStack();
            //Pops backstack once more later down the flow (see onStateChanged())
            setBgState(Background.STATE_SCREEN);
            return true;
        }

        return false;
    }

    /**
     * Begins the animation of Home As Up indicator.
     */
    private void startHomeAsUpAnim() {
        if(getSupportActionBar() == null)
            return; //Don't bother doing anything if App can't find ActionBar

        AnimatedVectorDrawableCompat d = (AnimatedVectorDrawableCompat)getAnimVectorDrawable(
                SerialSearchActivity.this, getHomeAsUpRes()
        );

        if(d != null) {
            getSupportActionBar().setHomeAsUpIndicator(d);
            d.start();
        }
    }

    /**
     * Fires when app receives data back from server. Begins the transition workflow to {@link
     * FragmentResult}
     */
    private void beginTransitionToResults() {
        setBgState(Background.STATE_BANNER);

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(
                        R.anim.fragment_transition_in,
                        R.anim.fragment_transition_out,
                        R.anim.fragment_transition_in,
                        R.anim.fragment_transition_out
                )
                .remove(f)
                .addToBackStack(TAG_TRANSIT_TO_BLANK)
                .commit();
    }

    /**
     * Fires when {@link Background} animation finishes. Refer to class Javadoc for more info.
     *
     * @param state is of primitive type int and is the state of this view once animation ends.
     */
    @Override
    public void onStateChangeEnded(int state) {
        if(getHomeAsUpRes() != R.drawable.vector_anim_r_menu) {
            startHomeAsUpAnim();
            setHomeAsUpRes(R.drawable.vector_anim_r_menu);
//            mHomeIndicatorRes = R.drawable.vector_anim_r_menu;
        } else {
            startHomeAsUpAnim();
            setHomeAsUpRes(R.drawable.vector_anim_menu);
//            mHomeIndicatorRes = R.drawable.vector_anim_menu;
        }

        //Places/Replaces FragmentResult into fragment container if Background state changed
        // to STATE_BANNER
        if(state == Background.STATE_BANNER) {
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(
                            R.anim.fragment_transition_in,
                            R.anim.fragment_transition_out,
                            R.anim.fragment_transition_in,
                            R.anim.fragment_transition_out
                    )
                    .replace(R.id.fragment_container, FragmentResult.newInstance(getResultList()))
                    .addToBackStack(TAG_TRANSIT_TO_RESULT)
                    .commit();
            //Pops backstack if Background state changed to STATE SCREEN
        } else if(state == Background.STATE_SCREEN) getSupportFragmentManager().popBackStack();
    }

    /**
     * Implemented from {@link VolleySerial.Callback}. Retrieves {@link SectionType} with id {@param
     * id}
     *
     * @param id is of primitive type long and is the ID of the section type to retrieve.
     * @return is of type {@link SectionType}.
     */
    @Override
    public SectionType getSectionType(long id) {
        return ((Universal)getApplicationContext()).getSectionType(id);
    }

    /**
     * Fires when {@link VolleySerial} ends in success.
     *
     * @param newSearch   is of primitive type boolean and is true when search is brand new (typed
     *                    in).
     * @param sectionList is of type {@link List<Section>} and is a nullable list of search
     *                    results.
     */
    @Override
    public void onResponseSuccess(boolean newSearch, @NonNull List<Section> sectionList) {
        //For when response received successfully but the sectionList is empty
        if(sectionList.isEmpty())
            onResponseFail(getString(R.string.text_no_show_default), ACTION_NONE);
        else {
            setResultList(sectionList);
            onAnyResponse();

            if(newSearch) storeSearch(sectionList.get(0));
            beginTransitionToResults();
        }
    }

    /**
     * Fires when {@link VolleySerial} ends in failure.
     */
    @Override
    public void onResponseFail(String message, int snackbarAction) {
        onAnyResponse();
        showSnackbar(message, snackbarAction);
    }

    /**
     * Method that aggregates all actions that need to be taken when any kind of response is
     * received through Volley.
     */
    private void onAnyResponse() {
        hideProgress();
        closeDrawer();
        resetUserInput();
    }

    /**
     * First method to fire within the sequence of searching for a serial number. Closes on-screen
     * keyboard if necessary.
     *
     * @param newSearch    is of primitive type boolean and is true when search is brand new (typed
     *                     in).
     * @param serialNum    is non-null and of type {@link String} and is the serial number to
     *                     search for.
     * @param requestToken is of type {@link IBinder} and nullable. Used when closing the on-screen
     *                     keyboard.
     */
    public void searchSerialNumber(boolean newSearch, @NonNull String serialNum,
                                   @Nullable IBinder requestToken, ContentLoadingProgressBar progress) {
        closeKeyboard(requestToken);
        showProgress(progress);

        Requester.getInstance(getApplicationContext()).requestSn(newSearch, serialNum, SerialSearchActivity.this);
    }

    /**
     * Shows {@link DialogDetails}
     *
     * @param s is of type {@link Section} and is the section for which this dialog is/was shown.
     */
    public void showDetailsDialog(Section s) {
        FragmentTransaction ft = dismissDialogs(false);
        DialogWrapper f = DialogDetails.newInstance(s.getItemList());
        f.show(ft, TAG_DIALOG);
    }

    /**
     * Shows {@link DialogRegister}
     *
     * @param s is of type {@link Section} and is the section for which this dialog is/was shown.
     */
    public void showRegisterDialog(Section s) {
        FragmentTransaction ft = dismissDialogs(false);
        DialogWrapper f = DialogRegister.newInstance(s);
        setActivityInterface(f);
        f.show(ft, TAG_DIALOG);
    }

    /**
     * Shows {@link DialogClearHistory}
     */
    public void showClearDialog() {
        FragmentTransaction ft = dismissDialogs(false);
        DialogWrapper f = DialogClearHistory.newInstance();
        f.show(ft, TAG_DIALOG);
    }

    /**
     * Shows {@link DialogSnHelp}
     */
    public void showSnHelpDialog() {
        FragmentTransaction ft = dismissDialogs(false);
        DialogWrapper f = DialogSnHelp.newInstance();
        f.show(ft, TAG_DIALOG);
    }

    public void startDetailActivity() {
        Intent i = new Intent(SerialSearchActivity.this, DetailActivity.class);
        startActivity(i);
        overridePendingTransactionWithAnim();
    }

    /**
     * Starts a {@link WebViewActivity}.
     *
     * @param url is of type {@link String} and is the URL to open within the new Activity.
     */
    public void startWebViewActivity(String url) {
        Intent i = new Intent(SerialSearchActivity.this, WebViewActivity.class);
        i.putExtra(WebViewActivity.KEY_URL, url);
        startActivity(i);
        overridePendingTransactionWithAnim();
    }

    /**
     * Starts a {@link PdfViewActivity}.
     *
     * @param url is of type {@link String} and is the URL of the PDF to open within the new
     *            activity.
     */
    public void startPdfViewActivity(String url) {
        Intent i = new Intent(SerialSearchActivity.this, WebViewActivity.class);
        i.putExtra(PdfViewActivity.KEY_URL, url);
        startActivity(i);
        overridePendingTransactionWithAnim();
    }

    /**
     * Shadows method {@link #overridePendingTransition(int, int)} to enforce uniform animations.
     */
    private void overridePendingTransactionWithAnim() {
        overridePendingTransition(R.anim.activity_transition_child_in, R.anim.activity_transition_parent_out);
    }

    /**
     * Shadows method {@link UserInput#reset()} but only after checking for null.
     */
    private void resetUserInput() {
        Log.d("user input", "user input object is null: " + String.valueOf(mUserInput == null));
        if(mUserInput != null) mUserInput.reset();
    }

    /**
     * Closes keyboard if open
     *
     * @param requestToken is of type {@link IBinder} and is used to hide keyboard. Is nullable.
     * @return is of primitive type boolean and is true if keyboard was ordered to close.
     */
    private boolean closeKeyboard(@Nullable IBinder requestToken) {
        if(requestToken != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            if(imm != null) return imm.hideSoftInputFromWindow(requestToken, 0);
        }

        return false;
    }

    /**
     * Fires when it is required to store the search.
     *
     * @param sectionItem is of type {@link Section} and is non-null. It can be any one of the
     *                    {@link Section} objects that {@link VolleySerial} returns. Currently using
     *                    the very first (index = 0).
     */
    private void storeSearch(@NonNull Section sectionItem) {
        final String model = sectionItem.getModel();
        final String serial = sectionItem.getSerialNum();
        final long date = System.currentTimeMillis();

        //Stores search in DB
        final long id = ((Universal)getApplication()).storeSearch(model, serial, date);

        //Stores search in Drawer
        getDrawerStart().addAdapterItem(new HistoryItem(id, model, serial, date));
    }

    /**
     * Helper method for displaying Snackbar
     *
     * @param message is of type {@link String} and is the message to display on the Snackbar
     */
    //TODO: Make sure errors are user friendly
    public void showSnackbar(String message, final int action) {
        if(mContent == null) return;

        final Snackbar sb = Snackbar.make(mContent, message, Snackbar.LENGTH_SHORT);
        sb.getView().setBackgroundColor(Color.DKGRAY);

        if(action != ACTION_NONE)
            sb.setAction(R.string.snackbar_action_help, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sb.dismiss();

                    switch(action) {
                        case ACTION_SN_HELP:
                            showSnHelpDialog();
                            break;
                    }
                }
            });

        sb.show();
    }

    /**
     * Sets {@link #mProgress} to be used later (ie for hiding) and shadows method {@link
     * ContentLoadingProgressBar#show()} but only after checking for null.
     *
     * @param progress is of type {@link ContentLoadingProgressBar} and is the progress bar to
     *                 show.
     */
    private void showProgress(@Nullable ContentLoadingProgressBar progress) {
        mProgress = progress;
        if(mProgress != null) mProgress.show();
    }

    /**
     * Shadows method {@link ContentLoadingProgressBar#hide()} but only after checking for null.
     */
    private void hideProgress() {if(mProgress != null) mProgress.hide();}

    /**
     * Sets results list to {@link #mSectionList} for use elsewhere.
     *
     * @param sectionList is of type {@link List<Section>} and is the results list of the most
     *                    recently searched serial number.
     */
    private void setResultList(List<Section> sectionList) {mSectionList = sectionList;}

    /**
     * Retrieves results list.
     *
     * @return is of type {@link List<Section>} and is the results list of the most recently
     * searched serial number.
     */
    private List<Section> getResultList() {return mSectionList;}

    private void setActivityInterface(ActivityInterface ai) {mActivityInterface = ai;}

    private void setHomeAsUpRes(@DrawableRes int resId) {mHomeIndicatorRes = resId;}
    @DrawableRes private int getHomeAsUpRes() {return mHomeIndicatorRes;}

    /**
     * Dismisses dialogs with the option to commit transaction if desired.
     *
     * @param commit is of primitive type boolean and is true if transaction requires committing.
     * @return is of type {@link FragmentTransaction} and is the dialog dismissal transaction.
     */
    private FragmentTransaction dismissDialogs(boolean commit) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prevDialog = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG);

        if(prevDialog != null) ft.remove(prevDialog);
        ft.disallowAddToBackStack();

        if(commit) ft.commit();
        return ft;
    }

    /**
     * Retrieves drawer start fragment by ID.
     *
     * @return is of type {@link FragmentDrawerStart}
     */
    private FragmentDrawerStart getDrawerStart() {
        return (FragmentDrawerStart)getSupportFragmentManager().findFragmentById(R.id.drawer_start);
    }

    /**
     * Nullable. Retrieves animated drawable for HomeAsUp indicator.
     *
     * @param c   is of type {@link Context} and is the context of the calling method.
     * @param res is of primitive type int and is the drawable resource to create a drawable from.
     * @return is of type {@link Drawable}
     */
    @Nullable
    private static Drawable getAnimVectorDrawable(Context c, @DrawableRes int res) {
        return AnimatedVectorDrawableCompat.create(c, res);
    }
}
