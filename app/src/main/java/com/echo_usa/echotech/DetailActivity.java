package com.echo_usa.echotech;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Sets up Actionbar by setting up HomeAsUp button and removing title text
        final ActionBar a = getSupportActionBar();
        if(a != null) {
            a.setCustomView(R.layout.toolbar_logo);
            a.setDisplayShowCustomEnabled(true);

            a.setDisplayHomeAsUpEnabled(true);
            a.setHomeAsUpIndicator(R.drawable.ic_vector_back);
            a.setHomeActionContentDescription(R.string.description_home_action);

            a.setTitle(null);
        }

        //Sets status bar translucency.
        ((Universal)getApplication()).setStatusBarTranslucent(DetailActivity.this, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_transition_parent_in, R.anim.activity_transition_child_out);
    }
}
