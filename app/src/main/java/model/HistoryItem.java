package model;

/**
 * Created by Zeno Yuki on 3/28/18.
 *
 * Model class that holds information regarding search history when passing data from DB to views
 */
public class HistoryItem {
    private long mId;
    private String mModel;
    private String mSerial;
    private long mSearchDate;

    /**
     * Constructor.
     *
     * @param id     is of primitive type long and is the ID of this history item (generated on the
     *               device).
     * @param model  is of type {@link String} and is the name of the model searched.
     * @param serial is of type {@link String} and is the serial number of the search.
     * @param date   is of primitive type long and is the date in which the search was conducted.
     */
    public HistoryItem(long id, String model, String serial, long date) {
        setId(id);
        setModel(model);
        setSerial(serial);
        setSearchDate(date);
    }

    //Setters & Getters
    public long getId() {return mId;}
    public void setId(long id) {mId = id;}

    public String getModel() {return mModel;}
    public void setModel(String model) {mModel = model;}

    public String getSerial() {return mSerial;}
    public void setSerial(String serial) {this.mSerial = serial;}

    public long getSearchDate() {return mSearchDate;}
    public void setSearchDate(long date) {mSearchDate = date;}
}
