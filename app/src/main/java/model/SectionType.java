package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Zeno Yuki on 7/16/18.
 *
 * Stores data regarding Section Types.
 *
 * Implements {@link Parcelable} so instances of this class can be passed into new Fragment
 * instances.
 */
public class SectionType implements Parcelable {
    //Constants for when storing/retrieving boolean values from database and remote.
    public static final int TRUE = 1;
    public static final int FALSE = 0;

    private long mId;
    private String mName;
    private boolean mHasButtons;
    private int mOrder;

    /**
     * Creator object for {@link Parcelable}. Declares how to instantiate a new array of this type
     * and when an instance is "reconstructed" from a parcel.
     */
    public static final Parcelable.Creator<SectionType> CREATOR = new Creator<SectionType>() {
        @Override
        public SectionType createFromParcel(Parcel src) {return new SectionType(src);}

        @Override
        public SectionType[] newArray(int size) {return new SectionType[size];}
    };

    /**
     * Private constructor used in the reconstruction from a parcel.
     *
     * @param src is of type {@link Parcel} and is the data that the reconstructed instance will
     *            contain.
     */
    private SectionType(Parcel src) {
        setId(src.readLong());
        setName(src.readString());
        setHasButtons(src.readInt() == TRUE);
        setOrder(src.readInt());
    }

    /**
     * Implements from {@link Parcelable}. Describes content.
     *
     * @return is of primitive type int.
     */
    @Override
    public int describeContents() {return 0;}

    /**
     * Implements from {@link Parcelable}. Fires when a new an instance is being deconstructed into
     * a {@link Parcel}
     *
     * @param dst   is of type {@link Parcel} and is where the data is stored.
     * @param flags is of primitive type int.
     */
    @Override
    public void writeToParcel(Parcel dst, int flags) {
        dst.writeLong(getId());
        dst.writeString(getName());
        dst.writeInt(hasButtons() ? TRUE : FALSE);
        dst.writeInt(getOrder());
    }

    //Constructor
    public SectionType() {}

    //Setters & Getters
    public long getId() {return mId;}
    public void setId(long id) {mId = id;}

    public String getName() {return mName;}
    public void setName(String type) {mName = type;}

    public boolean hasButtons() {return mHasButtons;}
    public void setHasButtons(boolean hasButtons) {mHasButtons = hasButtons;}

    public int getOrder() {return mOrder;}
    public void setOrder(int order) {mOrder = order;}
}
