package model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeno Yuki on 4/6/18.
 *
 * Stores data regarding a Result Section. Should hold reference to one {@link SectionType} and a
 * list of {@link SectionValue}s.
 *
 * Implements {@link Parcelable} so instances of this class can be passed into new Fragment
 * instances.
 */
public class Section implements Parcelable {
    private String mSerialNum;
    private String mModel;
    private SectionType mType;
    private List<SectionValue> mItemList;

    /**
     * Creator object for {@link Parcelable}. Declares how to instantiate a new array of this type
     * and when an instance is "reconstructed" from a parcel.
     */
    public static final Parcelable.Creator<Section> CREATOR = new Parcelable.Creator<Section>() {
        @Override
        public Section createFromParcel(Parcel src) {return new Section(src);}

        @Override
        public Section[] newArray(int size) {return new Section[size];}
    };

    /**
     * Private constructor used in the reconstruction from a parcel.
     *
     * @param src is of type {@link Parcel} and is the data that the reconstructed instance will
     *            contain.
     */
    private Section(Parcel src) {
        setSerialNum(src.readString());
        setModel(src.readString());
        setType((SectionType)src.readParcelable(SectionType.class.getClassLoader()));

        mItemList = new ArrayList<>();
        src.readTypedList(mItemList, SectionValue.CREATOR);
    }

    /**
     * Implements from {@link Parcelable}. Describes content.
     *
     * @return is of primitive type int.
     */
    @Override
    public int describeContents() {return 0;}

    /**
     * Implements from {@link Parcelable}. Fires when a new an instance is being deconstructed into
     * a {@link Parcel}
     *
     * @param dst   is of type {@link Parcel} and is where the data is stored.
     * @param flags is of primitive type int.
     */
    @Override
    public void writeToParcel(Parcel dst, int flags) {
        dst.writeString(getSerialNum());
        dst.writeString(getModel());
        dst.writeParcelable(getType(), flags);
        dst.writeTypedList(getItemList());
    }

    //Constructor. Instantiates a new ArrayList to prevent issues with list being null
    public Section() {setItemList(new ArrayList<SectionValue>());}

    //Setters & Getters
    public void setSerialNum(String serialNum) {mSerialNum = serialNum;}
    public String getSerialNum() {return mSerialNum;}

    public void setModel(String model) {mModel = model;}
    public String getModel() {return mModel;}

    public void setType(SectionType type) {mType = type;}
    public SectionType getType() {return mType;}

    public void setItemList(@NonNull List<SectionValue> itemList) {mItemList = itemList;}
    @NonNull public List<SectionValue> getItemList() {return mItemList;}

    /**
     * Shadows method {@link List#add(Object)}.
     *
     * @param item is of type {@link SectionValue} and is the value to add to values list.
     */
    public void addItem(SectionValue item) {getItemList().add(item);}

    /**
     * Retrieves item count from values list.
     *
     * @return is of primitive type int and is the number of entries within the list.
     */
    public int getItemCount() {return mItemList.size();}
}
