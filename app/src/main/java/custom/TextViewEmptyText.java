package custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.echo_usa.echotech.R;

/**
 * Created by Zeno Yuki on 7/18/18.
 *
 * Uses text received from the parameters in {@link #setEmptyText(CharSequence)} to display "empty
 * view" content. This view is an attempt to resolve the fact that Adapters don't have easy access
 * to {@link Context}, making it difficult to retrieve string resources.
 */
public class TextViewEmptyText extends AppCompatTextView {
    //Constructor
    public TextViewEmptyText(Context context) {
        super(context);
    }

    //Constructor
    public TextViewEmptyText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //Constructor
    public TextViewEmptyText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Formats incoming text {@param text} to display a contextual message regarding an empty
     * state.
     *
     * @param text nullable and is of type {@link CharSequence}
     */
    public void setEmptyText(@Nullable CharSequence text) {
        text = text == null ? "thing" : " " + text;
        String emptyText = getContext().getResources().getString(R.string.text_no_value_show, text);

        setText(emptyText);
    }
}
