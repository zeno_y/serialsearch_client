package custom;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.echo_usa.echotech.R;

/**
 * Created by Zeno Yuki on 3/20/18.
 *
 * Supports custom views with helper methods. Also provides views with a loose structure through the
 * implementation of {@link ViewStructure}
 */

final class ViewSupport {
    protected static final int ALPHA_VISIBLE = 255;
    protected static final int ALPHA_OVERLAY = 120;
    protected static final int ALPHA_GONE = 0;

    protected static final long ANIM_DURATION_L = 350L;
    protected static final long ANIM_DURATION_M = 250L;
    protected static final long ANIM_DURATION_S = 190L;

    /**
     * Private constructure to prevent this class from instantiation
     */
    private ViewSupport() {}

    /**
     * Static method. Instantiates a new {@link StaticLayout} object with the parameters passed into
     * this method
     *
     * @param c is of type {@link CharSequence} and is the text to hold in the static layout object
     * @param p is of type {@link TextPaint} and is the paint object to use with this static layout
     * @param w is of primitive type int and is the maximum width allowed in this static layout
     * @param a is of type {@link android.text.Layout.Alignment} and is the alignment of the text in
     *          the static layout
     * @return is of type {@link StaticLayout} and is the newly instantiated static layout object
     */
    @Nullable
    static StaticLayout getTextLayout(CharSequence c, TextPaint p, int w, Layout.Alignment a) {
        if(w < 0) w = 0;
        if(c != null && p != null && a != null) return new StaticLayout(c, p, w, a, 1f, 0f, true);
        else return null;
    }

    /**
     * Static method that overloads {@link #getTextLayout(CharSequence, TextPaint,
     * Layout.Alignment)}. Instantiates a new {@link StaticLayout} object with the parameters passed
     * into this method. All textlayouts returned form this method have an aligment of {@link
     * Layout.Alignment#ALIGN_NORMAL}
     *
     * @param c is of type {@link CharSequence} and is the text to hold in the static layout object
     * @param p is of type {@link TextPaint} and is the paint object to use with this static layout
     * @param w is of primitive type int and is the maximum width allowed in this static layout
     * @return is of type {@link StaticLayout} and is the newly instantiated static layout object
     */
    @Nullable
    static StaticLayout getTextLayout(CharSequence c, TextPaint p, int w) {
        final Layout.Alignment a = Layout.Alignment.ALIGN_NORMAL;
        return getTextLayout(c, p, w, a);
    }

    /**
     * Static method. Instantiates a new {@link StaticLayout} object with the parameters passed on
     * from this method
     *
     * @param c is of type {@link CharSequence} and is the text to hold in the static layout object
     * @param p is of type {@link TextPaint} and is the paint object to use with this static layout
     * @param a is of type {@link android.text.Layout.Alignment} and is the alignment of the text in
     *          the static layout
     * @return is of type {@link StaticLayout} and is the newly instantiated static layout object
     */
    @Nullable
    static StaticLayout getTextLayout(CharSequence c, TextPaint p, Layout.Alignment a) {
        final int w = (int)(p.measureText(c.toString()) + 0.5f);
        return getTextLayout(c, p, w, a);
    }

    /**
     * Static method. Instantiates a new {@link StaticLayout} object with the parameters passed on
     * from this method
     *
     * @param c is of type {@link CharSequence} and is the text to hold in the static layout object
     * @param p is of type {@link TextPaint} and is the paint object to use with this static layout
     * @return is of type {@link StaticLayout} and is the newly instantiated static layout object
     */
    @Nullable
    static StaticLayout getTextLayout(@Nullable CharSequence c, @Nullable TextPaint p) {
        if(c == null || p == null) return null;

        final int w = (int)(p.measureText(c.toString()) + 0.5f);
        return getTextLayout(c, p, w, Layout.Alignment.ALIGN_NORMAL);
    }

    /**
     * Retrieves the height of one line for the given text paint {@param p}
     *
     * @param p is of type {@link TextPaint} and is the object to retrieve the height from
     * @return is of primitive type int and is the height of one line of the given text paint
     */
    static int getTextLineHeight(@Nullable TextPaint p) {
        if(p == null) return 0;
        final Paint.FontMetrics fMetrics = p.getFontMetrics();

        return Math.round(-fMetrics.top + fMetrics.bottom);
    }

    /**
     * Retrieves the width of one line for the given text {@param c} and textpaint {@param p}
     *
     * @param c is of type {@link CharSequence} and is the text to measure
     * @param p is of type {@link TextPaint} and is the paint object to use while measuring text
     * @return is of primitive type int and is the width of the one line text.
     */
    static int getOneLineWidth(@Nullable CharSequence c, @Nullable TextPaint p) {
        if(c == null || p == null) return 0;
        return (int)(p.measureText(c.toString()) + 0.5f);
    }

    /**
     * Static method. Instantiates a new {@link Paint} object with the parameters passed on from
     * this method
     *
     * @param c       is of type {@link Context} and is the context of the calling object
     * @param colorId is of primitive type int and is the color resource ID of the color this paint
     *                object should be
     * @return is of type {@link Paint} and is the newly instantiated paint object
     */
    @NonNull
    protected static Paint getPaintById(Context c, @ColorRes int colorId) {
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

        try {
            p.setColor(ContextCompat.getColor(c, colorId));
        } catch(Resources.NotFoundException e) {
            e.printStackTrace();
            p.setColor(ContextCompat.getColor(c, R.color.color_primary));
        }

        p.setStyle(Paint.Style.FILL);
        return p;
    }

    @NonNull
    protected static Paint getPaintByColor(@ColorInt int color) {
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

        p.setColor(color);
        p.setStyle(Paint.Style.FILL);
        return p;
    }

    /**
     * Static method. Instantiates a new {@link Paint} object with the parameters passed on from
     * this method
     *
     * @param c       is of type {@link Context} and is the context of the calling object
     * @param a       is of primitive type int and is the alpha value to set this paint object to
     * @param colorId is of primitive type int and is the color resource ID of the color this paint
     *                object should be
     * @return is of type {@link Paint} and is the newly instantiated paint object
     */
    @NonNull
    protected static Paint getPaintById(Context c, int a, @ColorRes int colorId) {
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

        try {
            p.setColor(ContextCompat.getColor(c, colorId));
        } catch(Resources.NotFoundException e) {
            e.printStackTrace();
            p.setColor(ContextCompat.getColor(c, R.color.color_primary));
        }

        p.setStyle(Paint.Style.FILL);
        p.setAlpha(a);
        return p;
    }

    /**
     * Static method. Instantiates a new {@link TextPaint} object with the parameters passed on from
     * this method
     *
     * @param c        is of type {@link Context} and is the context of the calling object
     * @param textSize is of primitive type int and is the textsize resource ID to set this
     *                 textpaint object to
     * @param colorId  is of primitive type int and is the color resource ID of the color this paint
     *                 object should be
     * @return is of type {@link Paint} and is the newly instantiated paint object
     */
    @NonNull
    protected static TextPaint getTextPaintObj(Context c, @DimenRes int textSize, @ColorRes int colorId) {
        TextPaint tp = new TextPaint(Paint.ANTI_ALIAS_FLAG);

        try {
            tp.setTextSize(c.getResources().getDimensionPixelSize(textSize));
            tp.setColor(ContextCompat.getColor(c, colorId));
        } catch(Resources.NotFoundException e) {
            tp.setTextSize(c.getResources().getDimensionPixelSize(R.dimen.textsize_default));
            tp.setColor(ContextCompat.getColor(c, R.color.color_primary));
        }

        return tp;
    }

    /**
     * Static method. Draws text on the given canvas
     *
     * @param c  is of type {@link Canvas} and is the canvas to draw the text on
     * @param sl is of type {@link StaticLayout} and is the text to draw
     * @param p  is of type {@link Point} and is the origin position of the text to draw
     */
    protected static void drawText(@NonNull Canvas c, @Nullable StaticLayout sl, @Nullable Point p) {
        if(sl != null && p != null) {
            c.save();
            c.translate(p.x, p.y);

            sl.draw(c);
            c.restore();
        }
    }

    /**
     * Static method. Draws rectangle on the given canvas
     *
     * @param c is of type {@link Canvas} and is the canvas to draw the rectangle on
     * @param r is of type {@link Rect} and is the rectangle to draw
     * @param p is of type {@link Paint} and is the paint object to use on the rectangle
     */
    static void drawRect(@NonNull Canvas c, @Nullable Rect r, @Nullable Paint p) {
        if(r != null && p != null) {
            c.drawRect(r, p);
        }
    }

    /**
     * Static method. Draws rounded rectangle on the given canvas
     *
     * @param c   is of type {@link Canvas} and is the canvas to draw the rectangle on
     * @param r   is of type {@link Rect} and is the rectangle to draw
     * @param p   is of type {@link Paint} and is the paint object to use on the rectangle
     * @param rad is of primitive type int and is the radius of the rounded corners
     */
    protected static void drawRoundedRect(@NonNull Canvas c, @Nullable Rect r, @Nullable Paint p, int rad) {
        if(r != null && p != null) {
            final RectF rF = new RectF(r);
            c.drawRoundRect(rF, rad, rad, p);
        }
    }

    /**
     * Static method. Draws bitmap on given canvas
     *
     * @param c  is of type {@link Canvas} and is the canvas to draw the bitmap on
     * @param bm is of type {@link Bitmap} and is the bitmap to draw
     * @param pt is of type {@link Point} and is the origin position of the bitmap to draw
     * @param p  is of type {@link Paint} and is the paint object to use on the bitmap
     */
    protected static void drawBitmapAtOrigin
    (@NonNull Canvas c, @Nullable Bitmap bm, @Nullable Point pt, @Nullable Paint p) {
        if(bm != null && pt != null) c.drawBitmap(bm, pt.x, pt.y, p);
    }

    /**
     * Static method. Retrieves the width of this device's screen
     *
     * @return is of primitive type int and is the width of this device's screen in pixels
     */
    protected static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    /**
     * Static method. Retrieves the height of this device's screen
     *
     * @return is of primitive type int and is the height of this device's screen in pixels
     */
    protected static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    /**
     * Determines device actionbar size
     *
     * @param context is of type {@link Context} and holds context of calling class.
     * @return is of primitive type int and is the size of this device's actionbar
     */
    public static int getActionBarSize(@NonNull Context context) {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = context.obtainStyledAttributes(typedValue.data, textSizeAttr);

        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();

        return actionBarSize;
    }

    /**
     * Determines device statusbar size
     *
     * @param context is of type {@link Context} and holds context of calling class.
     * @return is of primitive type int and is the size of this device's statusbar.
     */
    public static int getStatusBarSize(@NonNull Context context) {
        @DimenRes int resId = context.getResources().getIdentifier(
                "status_bar_height", "dimen", "android");
        try {
            return context.getResources().getDimensionPixelSize(resId);
        } catch(Resources.NotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Clamps scroll so the scrolling is limited to a given range
     *
     * @param s   is of primitive type int and is the current scroll value (x or y axis)
     * @param min is of primitive type int and is the minimum possible scroll value (must be of the
     *            same axis type as {@param s})
     * @param max is of primitive type int and is the maximum possible scroll value (must be of the
     *            same axis type as {@param s})
     * @return is of primitive type int and is the clamped scroll value
     */
    protected static int clampScroll(int s, int min, int max) {
        s = s < min ? min : s;
        s = s > max ? max : s;

        return s;
    }

    /**
     * Interface that provides implementing classes with a loose structure
     */
    protected interface ViewStructure {
        /**
         * Used in the constructors that is responsible for view initializing logic
         *
         * @param attrs Nullable object {@link AttributeSet} passed on from constructor param
         */
        void initialize(@Nullable AttributeSet attrs);

        /**
         * Provides the desired width of the implementing view
         *
         * @return is of primitive type int and is the desired width of the implementing view
         */
        int getDesiredWidth();

        /**
         * Provides the desired height of the implementing view
         *
         * @return is of primitive type int and is the desired height of the implementing view
         */
        int getDesiredHeight();

        /**
         * Updates content bounds for every view element
         *
         * @param availableW is of primitive type int and is the amount of width available for
         *                   elements to be placed in.
         * @param availableH is of primitive type int and is the amount of height available for
         *                   elements to be placed in.
         */
        void updateContentBounds(int availableW, int availableH);
    }
}
