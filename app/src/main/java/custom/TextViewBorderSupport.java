package custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.echo_usa.echotech.R;

/**
 * Created by Zeno Yuki on 5/9/18.
 *
 * Displays a {@link android.widget.TextView} with borders. Currently only supports bottom borders.
 */
public class TextViewBorderSupport extends AppCompatTextView implements ViewSupport.ViewStructure {
    //Object for bottom border
    private Rect mBtmBorderRect;
    //Object for all borders
    private Paint mBorderPaint;
    //Variable holds pixel size of border
    private int mBorderSize = 0;

    //Constructor
    public TextViewBorderSupport(Context context) {
        super(context);
        initialize(null);
    }

    //Constructor
    public TextViewBorderSupport(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    //Constructor
    public TextViewBorderSupport(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    /**
     * Sets up view for use.
     *
     * @param attrs Nullable object {@link AttributeSet} passed on from constructor param
     */
    @Override
    public void initialize(@Nullable AttributeSet attrs) {
        @ColorInt int borderColor = 0;

        if(attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(
                    attrs, R.styleable.border, 0, 0
            );
            mBorderSize = a.getDimensionPixelSize(R.styleable.border_border_size, 0);
            borderColor = a.getColor(R.styleable.border_border_color, 0);
        }

        if(borderColor != 0) mBorderPaint = ViewSupport.getPaintByColor(borderColor);
    }

    /**
     * Updates content bounds if width or height bounds of this view changes.
     *
     * @param w    is of primitive type int and is the new width of this view.
     * @param h    is of primitive type int and is the new height of this view.
     * @param oldw is of primitive type int and is the old width of this view.
     * @param oldh is of primitive type int and is the old height of this view.
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if(oldw != w || oldh != h) updateContentBounds(w, h);
    }

    /**
     * Draws the UI elements within this view.
     *
     * @param canvas is of type {@link Canvas} and is the canvas that the UI elements are drawn on.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        ViewSupport.drawRect(canvas, mBtmBorderRect, mBorderPaint);
    }

    /**
     * Updates content bounds of elements within this custom view.
     *
     * @param availableW is of primitive type int and is the amount of width available for elements
     *                   to be placed in.
     * @param availableH is of primitive type int and is the amount of height available for
     */
    @Override
    public void updateContentBounds(int availableW, int availableH) {
        final int l = getPaddingStart();
        final int r = availableW - getPaddingEnd();
        final int b = availableH;

        //Sets content bounds for bottom border.
        if(mBtmBorderRect == null) mBtmBorderRect = new Rect();
        mBtmBorderRect.set(l, b - mBorderSize, r, b);
    }

    //Unused Overrides from ViewSupport.ViewStructure
    @Override public int getDesiredWidth() {return 0;}
    @Override public int getDesiredHeight() {return 0;}
}
