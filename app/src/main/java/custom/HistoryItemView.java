package custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.echo_usa.echotech.R;

/**
 * Created by Zeno Yuki on 3/27/18.
 *
 * Simple custom view that was created to reduce the number of Views on screen at any given time.
 * Essentially is a grouping of multiple TextViews. Used in Drawer Start when displaying list of
 * search history. Currently only supports the display of bottom borders (as dividers)
 */
public class HistoryItemView extends View implements ViewSupport.ViewStructure {
    //Paint objects for displayed text
    private TextPaint mTextPaint;
    private TextPaint mSubtextPaint;

    //Objects for model name text
    private StaticLayout mModelLayout;
    private Point mModelOrigin;

    //Objects for serial number text
    private StaticLayout mSerialLayout;
    private Point mSerialOrigin;

    //Objects for date searched text
    private StaticLayout mDateLayout;
    private Point mDateOrigin;

    //Object for bottom border
    private Rect mBtmBorderRect;
    //Object for all borders
    private Paint mBorderPaint;
    //Variable holds pixel size of border
    private int mBorderSize = 0;

    //Constructor
    public HistoryItemView(Context context) {
        super(context);
        initialize(null);
    }

    //Constructor
    public HistoryItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    //Constructor
    public HistoryItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    //Constructor
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HistoryItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(attrs);
    }

    /**
     * Sets up view for use.
     *
     * @param attrs Nullable object {@link AttributeSet} passed on from constructor param
     */
    @Override
    public void initialize(@Nullable AttributeSet attrs) {
        @ColorInt int borderColor = 0;
        if(attrs != null) {
            TypedArray a = getContext().getTheme()
                    .obtainStyledAttributes(attrs, R.styleable.border, 0, 0);
            mBorderSize = a.getDimensionPixelSize(R.styleable.border_border_size, 0);
            borderColor = a.getColor(R.styleable.border_border_color, 0);
        }

        //Initialize Paint Objects
        if(borderColor != 0) mBorderPaint = ViewSupport.getPaintByColor(borderColor);

        //Initialize TextPaint objects
        mTextPaint = ViewSupport
                .getTextPaintObj(getContext(), R.dimen.textsize_default, R.color.textcolor_primary);
        mSubtextPaint = ViewSupport
                .getTextPaintObj(getContext(), R.dimen.textsize_default, R.color.textcolor_secondary);
    }

    /**
     * Sets texts for this view. Once layouts are created for the texts, updates content bounds and
     * invalidates.
     *
     * @param model  is of type {@link CharSequence} and is the text for model name
     * @param serial is of type {@link CharSequence} and is the text for serial number
     * @param date   is of type {@link CharSequence} and is the text for search date
     */
    public void setTexts(CharSequence model, CharSequence serial, CharSequence date) {
        mModelLayout = ViewSupport.getTextLayout(model, mTextPaint);
        mSerialLayout = ViewSupport.getTextLayout(serial, mTextPaint);

        date = getResources().getString(R.string.history_date_pretext, date);
        mDateLayout = ViewSupport.getTextLayout(date, mSubtextPaint);

        updateContentBounds(getWidth(), getHeight());
        invalidate();
    }

    /**
     * Measures view and fulfills the contract by calling {@link View#setMeasuredDimension(int,
     * int)}. Resolves desired dimensions with parent's spec/constraints
     *
     * @param widthMeasureSpec  is of primitive type int and is the parent's width spec/constraints
     *                          imposed on this child
     * @param heightMeasureSpec is of primitive type int and is the parent's height spec/constraints
     *                          imposed on this child
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int resolvedW = resolveSize(getDesiredWidth(), widthMeasureSpec);
        final int resolvedH = resolveSize(getDesiredHeight(), heightMeasureSpec);

        super.setMeasuredDimension(resolvedW, resolvedH);
    }

    /**
     * Updates content bounds when size of view changes.
     *
     * @param w    is of primitive type int and is the new width of this view
     * @param h    is of primitive type int and is the new height of this view
     * @param oldw is of primitive type int and is the old width of this view
     * @param oldh is of primtive type int and is the old height of this view
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if(oldw != w || oldh != h) updateContentBounds(w, h);
    }

    /**
     * Retrieves the desired width of this view.
     *
     * @return is of primitive type int and is the desired width of this view.
     */
    @Override
    public int getDesiredWidth() {return ViewSupport.getScreenWidth();}

    /**
     * Retrieves the desired height of this view.
     *
     * @return is of primitive type int and is the desired height of this view.
     */
    @Override
    public int getDesiredHeight() {return getHistoryItemSize();}

    /**
     * Draws the UI elements within this view.
     *
     * @param canvas is of type {@link Canvas} and is the canvas that the UI elements are drawn on.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        ViewSupport.drawText(canvas, mModelLayout, mModelOrigin);
        ViewSupport.drawText(canvas, mSerialLayout, mSerialOrigin);
        ViewSupport.drawText(canvas, mDateLayout, mDateOrigin);
        ViewSupport.drawRect(canvas, mBtmBorderRect, mBorderPaint);
    }

    /**
     * Updates the UI element's content bounds. Method provides width/height params to give the
     * ability to constrain elements to "trusted" dimensions
     *
     * @param availableW is of primitive type int and is the available width amount for the elements
     *                   to be placed within.
     * @param availableH is of primitive type int and is the available height amount for the
     *                   elements to be placed within.
     */
    @Override
    public void updateContentBounds(int availableW, int availableH) {
        final int textL = getPaddingStart();
        final int textT = getPaddingTop();
        final int textR = availableW - getPaddingRight();
        final int textB = availableH - getPaddingBottom();
        final int viewB = availableH;

        int x = textL;
        int y = textT;
        //Sets origin for model text
        if(mModelOrigin == null) mModelOrigin = new Point();
        mModelOrigin.set(x, y);

        x = textR - getSerialW();
        //Sets origin for serial number
        if(mSerialOrigin == null) mSerialOrigin = new Point();
        mSerialOrigin.set(x, y);

        x = textR - getDateW();
        y += ViewSupport.getTextLineHeight(mTextPaint);
        //Sets origin for search date
        if(mDateOrigin == null) mDateOrigin = new Point();
        mDateOrigin.set(x, y);

        if(mBtmBorderRect == null) mBtmBorderRect = new Rect();
        mBtmBorderRect.set(textL, viewB - mBorderSize, textR, viewB);
    }

    /**
     * Retrieves current serial number layout width.
     *
     * @return is of primitive type int and is the width of the current serial number layout object.
     */
    private int getSerialW() {
        return mSerialLayout != null ?
                ViewSupport.getOneLineWidth(mSerialLayout.getText(), mTextPaint) : 0;
    }

    /**
     * Retrieves current search date layout width.
     *
     * @return is of primitive type int and is the width of the current search date layout object.
     */
    private int getDateW() {
        return mDateLayout != null ?
                ViewSupport.getOneLineWidth(mDateLayout.getText(), mSubtextPaint) : 0;
    }

    /**
     * Retrieves desired height of this view.
     *
     * @return is of primitive type int and is the desired height of this view.
     */
    private int getHistoryItemSize() {
        return getResources().getDimensionPixelSize(R.dimen.history_item_size);
    }
}
