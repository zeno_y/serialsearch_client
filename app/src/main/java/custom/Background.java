package custom;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowInsets;

import com.echo_usa.echotech.R;

//import android.util.Log;

/**
 * Created by Zeno Yuki on 3/20/18.
 *
 * Handles the display and animation of the main background and logo. Adjusting view padding will
 * cause logo to adjust in size.
 */
public class Background extends View implements ViewSupport.ViewStructure {
    private int[] mInsets = new int[]{0, 0, 0, 0}; //in order of left, top, right, bottom
    //Flags to set mState when view contents changes in size
    public static final int STATE_BANNER = 0x1;
    public static final int STATE_SCREEN = 0x2;

    //Logo's top pos in relationship with view height
    private static final float LOGO_TOP_RATIO = 0.25f;

    //Keeps track of view status: up or down.
    private int mState = STATE_SCREEN;

    //Object for background gradient drawable
    private Drawable mGradient;
    //Object for logo drawable
    private Drawable mLogo;

    //Objects for logo underline
    private Rect mUnderlineRect;
    private Paint mUnderlinePaint;

    //Objects for background layer
    private Rect mBgRect;
    private Paint mBgPaint;

    //Objects for text below logo
    private int mLogoPadding = 0;
    private int mSubtitleW;
    private StaticLayout mSubtitleLayout;
    private Point mSubtitleOrigin;

    //Objects for animation
    private Animator mStatusChangeAnim;
    private int mElementHeight;

    //Object(s) for onStateChangeEnded
    private StateCallback mCallback;

    //Constructor
    public Background(Context context) {
        super(context);
        initialize(null);
    }

    //Constructor
    public Background(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    //Constructor
    public Background(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    //Constructor
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Background(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(attrs);
    }

    /**
     * Sets up view for use.
     *
     * @param attrs Nullable object {@link AttributeSet} passed on from constructor param
     */
    @Override
    public void initialize(@Nullable AttributeSet attrs) {
        //Retrieves custom attr values. Note that Typed Array is indeed being recycled.
        if(attrs != null) {
            TypedArray a = getContext().getTheme()
                    .obtainStyledAttributes(attrs, R.styleable.Background, 0, 0);
            mLogoPadding = a.getDimensionPixelSize(R.styleable.Background_logo_padding, mLogoPadding);

            a.recycle();
        }

        //Retrieve drawables
        mGradient = ContextCompat.getDrawable(getContext(), R.drawable.dr_bg_gradient);
        mLogo = VectorDrawableCompat
                .create(getResources(), R.drawable.ic_vector_techforce_logo, null);

        //Initialize paint objects
        mBgPaint = ViewSupport.getPaintById(getContext(), R.color.echo_orange);
        mUnderlinePaint = ViewSupport.getPaintById(getContext(), R.color.textcolor_dark);

        //Initialize objects for text
        CharSequence subtitle = getResources().getString(R.string.tech_companion);
        TextPaint subtitlePaint = ViewSupport
                .getTextPaintObj(getContext(), R.dimen.textsize_title, R.color.textcolor_dark);
        mSubtitleLayout = ViewSupport.getTextLayout(subtitle, subtitlePaint);

        //Measure text width and store as variable
        setSubtitleW(ViewSupport.getOneLineWidth(subtitle, subtitlePaint));
    }

    /**
     * Toggles size. More specifically, adjusts content bounds, through animation, depending on
     * status value at method execution. Sets {@link Background#mState} accordingly.
     */
    public void setContentState(int state) {
        if(state == getState())
            return; //prevent anything from changing if view is already in param state
        setState(state); //sets current state to new state provided by param

        //Determines how far to animate view elements from/to
        final int from = getState() == STATE_BANNER ? getHeight() : getElementHeightTarget();
        final int to = getState() == STATE_BANNER ? getElementHeightTarget() : getHeight();

        //Cancels animation if running, then runs animation.
        if(isAnimRunning()) mStatusChangeAnim.cancel();
        mStatusChangeAnim = getStateChangeAnim(Background.this, from, to);
        mStatusChangeAnim.start();
    }

    /**
     * Measures view and fulfills the contract by calling {@link View#setMeasuredDimension(int,
     * int)}. Resolves desired dimensions with parent's spec/constraints
     *
     * @param widthMeasureSpec  is of primitive type int and is the parent's width spec/constraints
     *                          imposed on this child
     * @param heightMeasureSpec is of primitive type int and is the parent's height spec/constraints
     *                          imposed on this child
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int hMode = MeasureSpec.getMode(heightMeasureSpec);
        final int hSize = MeasureSpec.getSize(heightMeasureSpec);

        final int hInset = mInsets[3] - mInsets[1];

        final int resolvedW = resolveSize(getDesiredWidth(), widthMeasureSpec);
        final int resolvedH = resolveSize(
                getDesiredHeight(), MeasureSpec.makeMeasureSpec(hSize - hInset, hMode)
        );

        super.setMeasuredDimension(resolvedW, resolvedH);
    }

    /**
     * Stores insets data for use in onMeasure. This is a work-around to force custom view to resize
     * when keyboard is present. This method works in tandem with view attr fitsSystemWindows="true"
     * and activity attr windowSoftInputMode="adjustResize". Suppresses ObsoleteSdkInt to prevent
     * warnings for wrapping method code in a SDK version check.
     *
     * @param insets is of type {@link WindowInsets} and holds the insets values for the window this
     *               view is located within.
     * @return is of type {@link WindowInsets} and is the adjusted window insets
     */
    @SuppressLint("ObsoleteSdkInt")
    @Override
    public WindowInsets onApplyWindowInsets(WindowInsets insets) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            mInsets[0] = insets.getSystemWindowInsetLeft();
            mInsets[1] = insets.getSystemWindowInsetTop();
            mInsets[2] = insets.getSystemWindowInsetRight();
            mInsets[3] = insets.getSystemWindowInsetBottom();

            return super.onApplyWindowInsets(insets.replaceSystemWindowInsets(
                    0, 0, 0, insets.getSystemWindowInsetBottom()
            ));
        } else return insets;
    }

    /**
     * Updates content bounds when size of view changes. Sets element height to be used in {@link
     * #updateContentBounds(int, int)} if current state is {@link #STATE_SCREEN} (meaning this view
     * must cover the entire screen).
     *
     * @param w    is of primitive type int and is the new width of this view
     * @param h    is of primitive type int and is the new height of this view
     * @param oldw is of primitive type int and is the old width of this view
     * @param oldh is of primtive type int and is the old height of this view
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if(getState() == STATE_SCREEN) setElementHeight(h);
        if(oldw != w || oldh != h) updateContentBounds(w, h);
    }

    /**
     * Retrieves the desired width of this view.
     *
     * @return is of primitive type int and is the desired width of this view.
     */
    @Override
    public int getDesiredWidth() {return ViewSupport.getScreenWidth();}

    /**
     * Retrieves the desired height of this view.
     *
     * @return is of primitive type int and is the desired height of this view.
     */
    @Override
    public int getDesiredHeight() {return ViewSupport.getScreenHeight();}

    /**
     * Draws the UI elements within this view.
     *
     * @param canvas is of type {@link Canvas} and is the canvas that the UI elements are drawn on.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        //background
        ViewSupport.drawRect(canvas, mBgRect, mBgPaint);
        //gradient
        mGradient.draw(canvas);
        //logo
        mLogo.draw(canvas);
        //logo underline
        ViewSupport.drawRect(canvas, mUnderlineRect, mUnderlinePaint);
        //subtitle
        ViewSupport.drawText(canvas, mSubtitleLayout, mSubtitleOrigin);
    }

    /**
     * Updates the UI element's content bounds. Method provides width/height params to allow for the
     * ability to constrain elements to "trusted" dimensions
     *
     * @param availableW is of primitive type int and is the available width amount for the elements
     *                   to be placed within.
     * @param availableH is of primitive type int and is the available height amount for the
     *                   elements to be placed within.
     */
    @Override
    public void updateContentBounds(int availableW, int availableH) {
        //Text (t) boundaries
        final int tL = mLogoPadding;
        final int tR = mLogoPadding;

        final int logoW = availableW - tL - tR;

        //background. Spans entire view.
        if(mBgRect == null) mBgRect = new Rect();
        mBgRect.set(0, 0, availableW, getElementHeight());

        //Uses constant variable to alter the rate in which the logo gets closer to the toolbar_logo
        //during animation.
        int vPos = Math.round(LOGO_TOP_RATIO * getElementHeight());
        //Prevents logo from going beyond actionbar height.
        vPos = Math.max(vPos, getMinimumLogoTop());

        //logo. Maintains aspect ratio while scaling to fit view boundaries, including padding
        if(mLogo != null) {
            final float r = (float)logoW / mLogo.getIntrinsicWidth();
            final int logoH = Math.round(mLogo.getIntrinsicHeight() * r);

            mLogo.setBounds(tL, vPos, availableW - tR, vPos + logoH);
            vPos += logoH + getPaddingHalf();
        }

        //logo underline. Spans logo, positioned right below
        if(mUnderlineRect == null) mUnderlineRect = new Rect();
        mUnderlineRect.set(tL, vPos, availableW - tR, vPos + getUnderlineH());
        vPos += getUnderlineH() + getPaddingHalf();

        //subtitle. Spans text length, centered with logo
        if(mSubtitleOrigin == null) mSubtitleOrigin = new Point();
        final int xSubtitle = Math.round((float)Math.abs(availableW - getSubtitleW()) / 2);
        mSubtitleOrigin.set(xSubtitle, vPos);

        //Gradient
        if(mGradient != null) {
            mGradient.setBounds(0, 0, availableW, getElementHeightTarget());
        }
    }

    /**
     * Shadows method {@link Animator#isRunning()}. Used in Activity to prevent certain buttons from
     * being clicked twice while animation is running.
     *
     * @return is of primitive type boolean and is true if animation is running.
     */
    public boolean isAnimRunning() {
        return mStatusChangeAnim != null && mStatusChangeAnim.isRunning();
    }

    /**
     * Animates view elements. Shifts elements to the top of the screen for content reveal.
     *
     * @param v    is of type {@link Background} and is the view that will experience the animation
     * @param from is of primitive type int and is the start height for view elements.
     * @param to   is of primitive type int and is the end height for view elements.
     * @return is of type {@link ValueAnimator} and is the animation object.
     */
    private static ValueAnimator getStateChangeAnim(final Background v, int from, int to) {
        ValueAnimator va = ValueAnimator.ofInt(from, to);
        va.setDuration(ViewSupport.ANIM_DURATION_L);
        va.setStartDelay(v.getAnimDelay());
        va.setInterpolator(new FastOutSlowInInterpolator());

        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final int i = (int)animation.getAnimatedValue();
                v.updateElementHeight(i);
            }
        });

        va.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.onStateChangeEnded();
            }
        });

        return va;
    }

    private void updateElementHeight(int height) {
        setElementHeight(height);

        updateContentBounds(getWidth(), getHeight());
        invalidate();
    }

    /**
     * Stores UI element height. This is a different value from VIEW HEIGHT. Used in animation.
     *
     * @param height is of primitive type int and is the current height of view elements.
     */
    private void setElementHeight(int height) {mElementHeight = height;}

    /**
     * Retrieves stored UI element height. This is a different value from VIEW HEIGHT. Used in
     * animation.
     *
     * @return is of primitive type int and is the overall height of view elements.
     */
    private int getElementHeight() {return mElementHeight;}

    /**
     * Retrieves the target height of the UI elements within this view after animation.
     *
     * @return is of primitive type int and is the target height of UI elements.
     */
    private int getElementHeightTarget() {
        return getResources().getDimensionPixelSize(R.dimen.background_banner_size)
                + ViewSupport.getStatusBarSize(getContext());
    }

    /**
     * Retrieves minimum distance from top of logo to top of screen.
     *
     * @return is of primitive type int and is the minimum distance mentioned above.
     */
    private int getMinimumLogoTop() {
        return ViewSupport.getStatusBarSize(getContext())
                + ViewSupport.getActionBarSize(getContext()) + getPadding1();
    }

    /**
     * Sets the subtitled width stored in {@link Background#mSubtitleW}
     *
     * @param w is of primitive type int and is the subtitle width to store.
     */
    private void setSubtitleW(int w) {mSubtitleW = w;}

    /**
     * Retrieves the subtitled width stored in {@link Background#mSubtitleW}
     *
     * @return is of primitive type int and is the subtitle width stored.
     */
    private int getSubtitleW() {return mSubtitleW;}

    /**
     * Sets state of this view.
     *
     * @param state is of primitive type int and is either {@link #STATE_SCREEN} or {@link
     *              #STATE_BANNER}
     */
    private void setState(int state) {mState = state;}

    /**
     * Retrieves state of this view.
     *
     * @return is of primitive type int and is either {@link #STATE_SCREEN} or {@link #STATE_BANNER}
     */
    private int getState() {return mState;}

    /**
     * Retrieves anim start delay. Resource is used as fragment transition animation length.
     *
     * @return is of primitive type long and is the amount of time in milliseconds to wait before
     * starting the animation
     */
    private long getAnimDelay() {return (long)getResources().getInteger(R.integer.anim_fragment_transition);}

    /**
     * Retrieves the underline height stored in resources.
     *
     * @return is of primitive type int and is the underline height.
     */
    private int getUnderlineH() {return getResources().getDimensionPixelSize(R.dimen.logo_underline);}

    /**
     * Retrieves the half padding value stored in resources.
     *
     * @return is of primitive type int and is the half padding value.
     */
    private int getPaddingHalf() {return getResources().getDimensionPixelSize(R.dimen.padding_half);}

    /**
     * Retrieves the one padding value stored in resources.
     *
     * @return is of primitive type int and is the one padding value.
     */
    private int getPadding1() {return getResources().getDimensionPixelSize(R.dimen.padding_1);}

    /**
     * Links class to receive callback to
     *
     * @param callback is of type {@link StateCallback} and is the implementing class
     */
    public void setCallback(StateCallback callback) {mCallback = callback;}

    /**
     * Shadows method {@link StateCallback#onStateChangeEnded(int)}
     */
    private void onStateChangeEnded() {
        if(mCallback != null) mCallback.onStateChangeEnded(getState());
    }

    /**
     * Callback interface for state changes
     */
    public interface StateCallback {
        /**
         * Fires when state change anim finishes running
         *
         * @param state is of primitive type int and is the state of this view once animation ends.
         * @see Background#getStateChangeAnim(Background, int, int)
         */
        void onStateChangeEnded(int state);
    }
}
