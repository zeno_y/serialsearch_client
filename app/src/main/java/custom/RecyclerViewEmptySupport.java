package custom;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Zeno Yuki on 5/3/18.
 *
 * Attempts to handle empty views like {@link android.widget.ListView}. Uses an {@link
 * RecyclerView.AdapterDataObserver} to figure out when Recycler content is changed and to display
 * the empty view if necessary.
 */
public class RecyclerViewEmptySupport extends RecyclerView {
    private View mEmptyView; //Empty view to display when RecyclerView is empty.
    //Listener/Observer to determine when the Recyclerview is empty
    private AdapterDataObserver mObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            //Adapter should never be null here
            Adapter<?> adapter = getAdapter();

            //Checks if adapter is null. OnChange my fire when adapter hasn't been set yet
            if(adapter != null) {
                final boolean isEmpty = adapter.getItemCount() < 1;
                if(mEmptyView != null) mEmptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
                RecyclerViewEmptySupport.this.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
            }
        }
    };

    //Constructor
    public RecyclerViewEmptySupport(Context context) {super(context);}

    //Constructor
    public RecyclerViewEmptySupport(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    //Constructor
    public RecyclerViewEmptySupport(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Stores a reference to the view passed through as a parameter.
     *
     * @param emptyView is of type {@link View} and is the empty view to display when necessary.
     */
    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
        getObserver().onChanged();
    }

    /**
     * Shadows method {@link RecyclerView.Adapter#registerAdapterDataObserver(AdapterDataObserver)}.
     *
     * Registers the data observer to the adapter.
     */
    public void registerAdapterObserver() {
        if(getAdapter() == null) return;
        getAdapter().registerAdapterDataObserver(getObserver());
        getObserver().onChanged();
    }

    /**
     * Shadows method {@link RecyclerView.Adapter#unregisterAdapterDataObserver(AdapterDataObserver)}.
     *
     * Unregisters a previously registered data observer.
     */
    public void unregisterAdapterObserver() {
        getAdapter().unregisterAdapterDataObserver(getObserver());
    }

    @NonNull private AdapterDataObserver getObserver() {return mObserver;}
}
