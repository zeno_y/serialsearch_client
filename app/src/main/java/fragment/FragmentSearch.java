package fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Editable;
import android.text.TextWatcher;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.echo_usa.echotech.R;
import com.echo_usa.echotech.SerialSearchActivity;

/**
 * Created by Zeno Yuki on 3/21/18.
 *
 * Sets up a fragment for accepting user input for search.
 */
public class FragmentSearch extends Fragment
        implements View.OnClickListener, TextView.OnEditorActionListener, UserInput, TextWatcher {
    private Button mSearchBtn, mQrBtn;
    private TextInputEditText mEditText;
    private ContentLoadingProgressBar mProgress;

    //Keeps track of whether or not a user input has been received. This is done to prevent
    // multiple consecutive clicks to an input, preventing consecutive Volley or Fragment
    // Transaction calls.
    private boolean mUserInputReceived = false;

    /**
     * Generates a new instance of this class. Used so that Framework is forced to "pass parameters"
     * using the newInstance() method instead of a bare constructor.
     *
     * This particular newInstance() method has no params.
     *
     * @return is of this type and is the newly instantiated dialog object.
     */
    public static FragmentSearch newInstance() {
        Bundle args = new Bundle();

        FragmentSearch fragment = new FragmentSearch();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires when fragment is called to "resume". Resets input-received variable so that fragment
     * will restart accepting user input.
     */
    @Override
    public void onResume() {
        super.onResume();
        setUserInputReceived(false);
    }

    /**
     * Fires to inflate the view for this fragment.
     *
     * @param inflater           is of type {@link LayoutInflater} and is the inflater used to
     *                           inflate the view.
     * @param container          is of type {@link ViewGroup} and is the parent view/viewgroup of
     *                           this fragment.
     * @param savedInstanceState is of type {@link Bundle} and is the previously saved state, if
     *                           any.
     * @return is of type {@link View} and is the inflated view to be used for this fragment.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    /**
     * Fires once view is created. Sets up view/UI elements here.
     *
     * @param view               is of type {@link View} and is the view inflated in {@link
     *                           #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is nullable and is of type {@link Bundle}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mQrBtn = (Button)view.findViewById(R.id.button_qrscan);
        mQrBtn.setOnClickListener(FragmentSearch.this);

        mSearchBtn = (Button)view.findViewById(R.id.button_search);
        mSearchBtn.setOnClickListener(FragmentSearch.this);
        mSearchBtn.setEnabled(false);

        mEditText = (TextInputEditText)view.findViewById(R.id.edit_text_serial);
        mEditText.setOnEditorActionListener(FragmentSearch.this);
        mEditText.addTextChangedListener(FragmentSearch.this);

        mProgress = (ContentLoadingProgressBar)view.findViewById(R.id.search_progress);
    }

    /**
     * Implements from {@link View.OnClickListener} Handles individual item clicks in the history
     * item list, the Home As Up button overlay ("overlay" because the drawer hides the original
     * button) and the history clear button.
     *
     * @param v is of type {@link View} and is the view that was clicked.
     */
    @Override
    public void onClick(View v) {
        //Prevent consecutive clicks here
        if(getUserInputReceived()) return;
        else setUserInputReceived(true);

        switch(v.getId()) {
            case R.id.button_qrscan:
                //TODO: do something on click
                break;
            case R.id.button_search:
                Log.d("user input", "clicked");
                searchSerialNumber(v);
                break;
        }
    }

    /**
     * Implements from {@link TextView.OnEditorActionListener}
     *
     * Fires when the soft keyboard is pressed. In this implementation, the method runs code only
     * when the "search" button is pressed.
     *
     * @param v        is of type {@link TextView} and is the view that was in focus during the key
     *                 press.
     * @param actionId is of primitive type int and is the ID of the key pressed.
     * @param event    is of type {@link KeyEvent} and is the object associated to the key press.
     * @return is of primitive type boolean and is true if the action was consumed.
     */
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_SEARCH) {
            if(v.getText().length() < 1) return true; //Consume to keep keyboard open

            if(getUserInputReceived()) return false;
            else setUserInputReceived(true);

            searchSerialNumber(v);
            return true;
        }

        return false;
    }

    /**
     * Implements from {@link TextWatcher}
     *
     * Sets search button as enabled depending on text input.
     *
     * @param s is of type {@link Editable} and is the text after the textchange.
     */
    @Override
    public void afterTextChanged(Editable s) {mSearchBtn.setEnabled(s.length() > 0);}

    /**
     * Implements from {@link UserInput} Resets variable responsible for tracking user input.
     */
    @Override
    public void reset() {
        setUserInputReceived(false);
    }

    /**
     * Searches for the serial number input by the user.
     *
     * @param v is of type {@link View} and is the view that is associated to the show keyboard
     *          request.
     */
    private void searchSerialNumber(View v) {
        final String errorMsg = getString(R.string.snackbar_empty_search);

        //Checks for null. If true, show user error message.
        if(mEditText.getText() == null) {
            ((SerialSearchActivity)getActivity()).showSnackbar(
                    errorMsg, SerialSearchActivity.ACTION_NONE
            );
            return;
        }

        final String inputSerial = mEditText.getText().toString();

        //Checks for empty string. If true, show user error message.
        if(inputSerial.isEmpty()) {
            ((SerialSearchActivity)getActivity()).showSnackbar(
                    errorMsg, SerialSearchActivity.ACTION_NONE
            );
            return;
        }

        //Searches serial number
        ((SerialSearchActivity)getActivity()).searchSerialNumber(
                true, inputSerial, v.getWindowToken(), mProgress
        );
    }

    //Setters & Getters
    private void setUserInputReceived(boolean received) {mUserInputReceived = received;}

    private boolean getUserInputReceived() {return mUserInputReceived;}

    //Unused overrides from interface TextWatcher
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}
}
