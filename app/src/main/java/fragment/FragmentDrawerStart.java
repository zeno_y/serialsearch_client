package fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.echo_usa.echotech.R;
import com.echo_usa.echotech.SerialSearchActivity;
import com.echo_usa.echotech.Universal;

import java.util.List;

import adapter.HistoryAdapter;
import custom.RecyclerViewEmptySupport;
import model.HistoryItem;

/**
 * Created by Zeno Yuki on 3/22/18.
 *
 * Sets up a static fragment for the Drawer Start of this application. Since static, newInstance()
 * method is not provided.
 *
 * Users can initiate a search through this drawer by clicking on an item available in the history
 * list. UI will only change after drawer is closed in this case.
 *
 * Each history item view within the Recyclerview holds a reference to the click listener
 * implemented in this class. The listener is passed through the adapter constructor.
 */
public class FragmentDrawerStart extends Fragment
        implements View.OnClickListener, DrawerLayout.DrawerListener {
    private Button mClearBtn;
    private RecyclerViewEmptySupport mRecycler;
    private HistoryAdapter mAdapter;
    private ContentLoadingProgressBar mProgress;

    //Keeps track of whether or not the adapter for this drawer list requires dataset change
    // notification. This is done to prevent immediate changes to the dataset in certain
    // situations (ie prevents the following: search is initiated through the drawer and searched
    // item immediately has its search date changed)
    private boolean mRequiresNotify = false;
    //Keeps track of whether or not this drawer requires focus. This is done to prevent TextView
    // cursor from displaying behind the drawer when drawer is opened while SN search TextView is
    // selected.
    private boolean mRequiresFocus = true;
    //Keeps track of whether or not a user input has been received. This is done to prevent
    // multiple consecutive clicks to an input, preventing consecutive Volley or Fragment
    // Transaction calls.
    private boolean mUserInputReceived = false;

    /**
     * Fires to inflate the view for this fragment.
     *
     * @param inflater           is of type {@link LayoutInflater} and is the inflater used to
     *                           inflate the view.
     * @param container          is of type {@link ViewGroup} and is the parent view/viewgroup of
     *                           this fragment.
     * @param savedInstanceState is of type {@link Bundle} and is the previously saved state, if
     *                           any.
     * @return is of type {@link View} and is the inflated view to be used for this fragment.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_drawer_start, container, false);
    }

    /**
     * Fires once view is created. Sets up view/UI elements here.
     *
     * @param view               is of type {@link View} and is the view inflated in {@link
     *                           #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is nullable and is of type {@link Bundle}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecycler = (RecyclerViewEmptySupport)view.findViewById(R.id.drawer_start_list);
        mRecycler.setHasFixedSize(true); //All items use the same layout
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        List<HistoryItem> list = ((Universal)getActivity().getApplication()).getSearchHistoryList();

        mAdapter = new HistoryAdapter(list, FragmentDrawerStart.this, R.layout.item_history);
        mRecycler.setAdapter(mAdapter);

        //Sets empty view to RecyclerView
        TextView emptyView = view.findViewById(R.id.list_empty_view);
        emptyView.setText(R.string.text_no_history);
        mRecycler.setEmptyView(emptyView);

        mClearBtn = view.findViewById(R.id.drawer_start_clear);
        mClearBtn.setOnClickListener(FragmentDrawerStart.this);
        mClearBtn.setEnabled(!mAdapter.isEmpty());

        mProgress = (ContentLoadingProgressBar)view.findViewById(R.id.drawer_progress);

        ImageButton backBtn = (ImageButton)view.findViewById(R.id.drawer_start_back);
        backBtn.setOnClickListener(FragmentDrawerStart.this);
    }

    /**
     * Fires when fragment is called to "resume". Resets input-received variable so that fragment
     * will restart accepting user input. Also registers data observer within the RecyclerView.
     */
    @Override
    public void onResume() {
        super.onResume();
        setUserInputReceived(false);
        Log.d("listeners", "Adapter Observer registered at FragmentDS.onResume()");
        mRecycler.registerAdapterObserver();
    }

    /**
     * Fires when fragment is called to "pause". Unregisters the previously registered data
     * observer.
     */
    @Override
    public void onPause() {
        super.onPause();
        Log.d("listeners", "Adapter Observer unregistered at FragmentDS.onPause()");
        mRecycler.unregisterAdapterObserver();
    }

    /**
     * Implements from {@link View.OnClickListener} Handles individual item clicks in the history
     * item list, the Home As Up button overlay ("overlay" because the drawer hides the original
     * button) and the history clear button.
     *
     * @param v is of type {@link View} and is the view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            //Individual history item
            case R.id.item_history:
                //Prevents consecutive clicks
                if(getUserInputReceived()) return;
                else setUserInputReceived(true);

                //Retrieves HistoryItem from view tag. See HistoryAdapter for details.
                HistoryItem item = (HistoryItem)v.getTag();

                if(item != null) {
                    setRequiresNotify(true);

                    final long date = ((SerialSearchActivity)getActivity()).updateAndGetDate(item.getId());
                    //Adapter is notified at a later time of the dataset change.
                    updateAdapterItem(item.getId(), date);

                    ((SerialSearchActivity)getActivity()).searchSerialNumber(
                            false, item.getSerial(), null, mProgress
                    );
                }
                break;
            case R.id.drawer_start_clear:
                ((SerialSearchActivity)getActivity()).showClearDialog();
                break;
            case R.id.drawer_start_back:
                ((SerialSearchActivity)getActivity()).closeDrawer();
                break;
        }
    }

    /**
     * Implements from {@link DrawerLayout.DrawerListener} Fires when drawer comes to a close.
     * Notifies dataset changed and resets variables keeping track of certain states.
     *
     * @param drawerView is of type {@link View} and is the drawer that just closed.
     */
    @Override
    public void onDrawerClosed(View drawerView) {
        notifyAdapterDataSetChanged();
        setUserInputReceived(false);
        setRequiresFocus(true);
    }

    /**
     * Implements from {@link DrawerLayout.DrawerListener}. Continuously fires when drawer is
     * sliding. If determined that focus is required, request focus and set "requires focus?"
     * variable to false.
     *
     * @param drawerView  is of type {@link View} and is the view of the sliding drawer.
     * @param slideOffset is of primitive type float and is a value from 0.0 to 1.0 identifying the
     *                    current location of the drawer.
     */
    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        if(requiresFocus()) {
            drawerView.requestFocus();
            setRequiresFocus(false);
        }
    }

    /**
     * Adds item to adapter list. Immediately notifies adapter of dataset change.
     *
     * @param item is non-null and is of type {@link HistoryItem} and is the new item to add to the
     *             list.
     */
    public void addAdapterItem(@NonNull HistoryItem item) {
        if(mAdapter != null) {
            mAdapter.addAsFirst(item);
            mAdapter.notifyDataSetChanged();

            mClearBtn.setEnabled(!mAdapter.isEmpty());
        }
    }

    /**
     * Clears adapter list. Immediately notifies adapter of dataset change.
     */
    public void clearListAdapter() {
        if(mAdapter != null && mAdapter.getItemCount() > 0) {
            mAdapter.clearList();
            mAdapter.notifyDataSetChanged();
        }
        if(mClearBtn != null) mClearBtn.setEnabled(false);
    }

    /**
     * Shadows method Adapter's notifyDataSetChanged() method, but only after testing whether
     * notification is truly required.
     */
    public void notifyAdapterDataSetChanged() {
        if(mAdapter != null && requiresNotify()) {
            mAdapter.notifyDataSetChanged();
            setRequiresNotify(false);
        }
    }

    /**
     * Updates one item's date with {@param date} within the adapter with ID {@param id}. Adapter is
     * notified of the dataset change at a later time (when drawer is closed).
     *
     * @param id   is of primitive type long and is the ID of the item to edit.
     * @param date is of primitive type long and is the new date of the item.
     */
    private void updateAdapterItem(long id, long date) {
        if(mAdapter != null) mAdapter.updateItem(id, date);
    }

    //Setters & Getters
    private void setRequiresFocus(boolean required) {mRequiresFocus = required;}
    private boolean requiresFocus() {return mRequiresFocus;}

    private void setRequiresNotify(boolean required) {mRequiresNotify = required;}
    private boolean requiresNotify() {return mRequiresNotify;}

    private void setUserInputReceived(boolean received) {mUserInputReceived = received;}
    private boolean getUserInputReceived() {return mUserInputReceived;}

    //Unused overrides from interface DrawerLayout.DrawerListener
    @Override public void onDrawerStateChanged(int newState) {}
    @Override public void onDrawerOpened(View drawerView) {}
}
