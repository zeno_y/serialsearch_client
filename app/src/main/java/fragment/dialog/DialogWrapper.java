package fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.echo_usa.echotech.ActivityInterface;
import com.echo_usa.echotech.R;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE;

/**
 * Created by Zeno Yuki on 4/18/18.
 *
 * Super class intended as a super for all dialog classes in this mobile application.
 *
 * This is done for code and UI/UX uniformity.
 */
public abstract class DialogWrapper extends DialogFragment
        implements View.OnClickListener, ActivityInterface {
    private Button mPosBtn, mNegBtn;
    private ContentLoadingProgressBar mProgress;

    @Nullable
    private InputMethodManager mInputManager; //Used to open/close on-screen keyboard
    private boolean mIsSoftInputOpen; //Used to determine whether on-screen keyboard is open

    /**
     * Overrides {@link DialogFragment#onStart()} Sets Dialog's window layout params via {@link
     * #getWidthLayoutParams()} & {@link #getHeightLayoutParams()}
     */
    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();

        if(d == null) return;
        Window w = d.getWindow();

        if(w == null) return;
        w.setLayout(getWidthLayoutParams(), getHeightLayoutParams());
    }

    /**
     * Overrides {@link DialogFragment#onCreate(Bundle)} <p> Retrieves {@link InputMethodManager}
     * from {@link android.support.v7.app.AppCompatActivity#getSystemService(String)} <p> Sets
     * boolean variable {@link #mIsSoftInputOpen} to false
     *
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's previous
     *                           state
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInputManager = getActivity() != null ?
                (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE) : null;
        mIsSoftInputOpen = false;
    }

    /**
     * Overrides {@link DialogFragment#onCreateDialog(Bundle)} <p> Retrieves dialog object from
     * super and removes title window feature.
     *
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's previous
     *                           state
     * @return is of type {@link Dialog} and is the dialog object that this class displays
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return d;
    }

    /**
     * Overrides {@link DialogFragment#onCreateView(LayoutInflater, ViewGroup, Bundle)} <p> Inflates
     * the wrapper view, then wraps the inflated content view(s) (layout ID retrieved from {@link
     * #getContentLayouts()})
     *
     * @param inflater           is of type {@link LayoutInflater} and is the inflater instance used
     *                           to inflate wrapper and content view
     * @param container          is of type {@link ViewGroup} and is the parent viewgroup of this
     *                           dialog
     * @param savedInstanceState is of type {@link Bundle} and
     * @return is of type {@link View} and contains the wrapper view and content view
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //If activity isn't null, retrieves Activity's inflater. Using the former will ensure Activity's theme is used
        LayoutInflater resolvedInflater = getActivity() != null ? getActivity().getLayoutInflater() : inflater;
        ViewGroup wrapper = (ViewGroup)resolvedInflater.inflate(R.layout.dialog_wrapper, container, true);

        //Inflates last view first so that the views can be inserted at the very top of the
        // children list, every iteration of the loop.
        final int size = getContentLayouts().length;
        for(int i = size - 1; i >= 0; i--) {
            View content = inflater.inflate(getContentLayouts()[i], wrapper, false);
            if(content != null) wrapper.addView(content, 0);
        }

        return wrapper;
    }

    /**
     * Sets up title view, positive and negative views located in the wrapper viewgroup.
     *
     * @param view               is of type {@link View} and is the view returned from {@link
     *                           #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's saved state
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mPosBtn = (Button)view.findViewById(R.id.dialog_pos_btn);
        mNegBtn = (Button)view.findViewById(R.id.dialog_neg_btn);

        mProgress = (ContentLoadingProgressBar)view.findViewById(R.id.dialog_progress);

        mPosBtn.setOnClickListener(this);
        mNegBtn.setOnClickListener(this);

        showProgress(false); //By default all dialogs extending this class will not have progress shown initially.
    }

    /**
     * Implemented from {@link ActivityInterface}
     *
     * @param activityHasFocus True when Activity changed to have focus
     */
    @Override
    public void onActivityWindowFocusChanged(boolean activityHasFocus) {
        if(!activityHasFocus) {
            View v = getViewForSoftInput();
            if(v != null && mInputManager != null) {
                mInputManager.showSoftInput(v, SOFT_INPUT_STATE_VISIBLE);
                mIsSoftInputOpen = true;
            }
        }
    }

    /**
     * Closes on-screen keyboard
     *
     * @param btnView is of type {@link View} and is used in closing the on-screen keyboard
     */
    private void closeSoftInput(@NonNull View btnView) {
        IBinder iBinder = btnView.getWindowToken();

        if(iBinder != null && mInputManager != null) {
            mInputManager.hideSoftInputFromWindow(iBinder, 0);
        }
    }

    /**
     * Implemented from {@link android.view.View.OnClickListener} and passes the click event to
     * {@link #onBtnClick(int)} for subclasses to handle. Is final to prevent subclasses from
     * overriding
     *
     * @param v is of type {@link View} and is the view that consumed the user click.
     */
    @Override
    public final void onClick(View v) {
        if(mIsSoftInputOpen) {
            closeSoftInput(v);
            mIsSoftInputOpen = false;
        }

        onBtnClick(v.getId());
    }

    /**
     * Shows/Hides progress view depending on param input.
     *
     * @param show is of primitive type boolean and is true if the desire is to show the progress.
     */
    protected void showProgress(boolean show) {
        if(mProgress == null) return;

        if(show) mProgress.show();
        else mProgress.hide();
    }

    /**
     * Enables/Disables positive button
     *
     * @param enable is of primitive type boolean and is true if positive button is to be enabled
     */
    protected void enablePosBtn(boolean enable) {
        if(mPosBtn != null) mPosBtn.setEnabled(enable);
    }

    /**
     * Sets positive button text.
     *
     * @param textRes is of primitive type int and is the string resource of the button text.
     */
    protected void setPosBtnText(@StringRes int textRes) {
        if(mPosBtn != null) mPosBtn.setText(textRes);
    }

    /**
     * Sets positive button visibility.
     *
     * @param visibility is of primitive type int and is {@link View#VISIBLE}, {@link
     *                   View#INVISIBLE} or {@link View#GONE}.
     */
    protected void setPosBtnVisibility(int visibility) {
        if(mPosBtn != null) mPosBtn.setVisibility(visibility);
    }

    /**
     * Retrieves layout resource(s) that are inflated one by one in {@link
     * #onCreateView(LayoutInflater, ViewGroup, Bundle)}. This was done to accommodate the multiple
     * TextViews in {@link DialogDetails}. Resources declared here will display horizontally, in
     * ascending order (ie array position 0 == viewgroup position 0).
     *
     * @return is of primitive array, type int and can hold one or more layout resources.
     */
    @LayoutRes
    protected abstract int[] getContentLayouts();

    /**
     * Retrieves subclass' width Layout Parameter
     *
     * @return is of primitive type int and can either be {@link android.view.ViewGroup.LayoutParams#MATCH_PARENT}
     * or {@link android.view.ViewGroup.LayoutParams#WRAP_CONTENT}
     */
    protected abstract int getWidthLayoutParams();

    /**
     * Retrieves subclass' height Layout Parameter
     *
     * @return is of primitive type int and can either be {@link android.view.ViewGroup.LayoutParams#MATCH_PARENT}
     * or {@link android.view.ViewGroup.LayoutParams#WRAP_CONTENT}
     */
    protected abstract int getHeightLayoutParams();

    /**
     * Passes button click event to subclass'. Decided to provide this abstract method to force all
     * extended classes to handle button clicks, instead of allowing the choice to ignore the {@link
     * View.OnClickListener} implemented by this class.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button, which is either
     *              {@link R.id#dialog_neg_btn} or {@link R.id#dialog_pos_btn}
     */
    protected abstract void onBtnClick(@IdRes int btnId);

    /**
     * Retrieves the view from subclasses to use in opening on-screen keyboard. Keyboard does not
     * open if this returns null.
     *
     * @return is of type {@link View} and is nullable.
     */
    @Nullable
    protected abstract View getViewForSoftInput();
}
