package fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.echo_usa.echotech.R;

import java.util.List;

import model.Section;
import model.SectionValue;

/**
 * Created by Zeno Yuki on 4/25/18.
 *
 * Extends {@link DialogWrapper} to maintain uniformity both in the code and visually.
 *
 * Sets up a dialog to receive user input regarding a brand new registration
 */
//TODO: unfinished.
public class DialogRegister extends DialogWrapper {
    /**
     * Generates a new instance of this class. Used so that Framework is forced to "pass parameters"
     * using the newInstance() method instead of a bare constructor.
     *
     * Stores the param as a {@link Bundle} for later access.
     *
     * @param regSection is non-null and is of type {@link Section}.
     * @return is of this type and is the newly instantiated dialog object.
     */
    public static DialogRegister newInstance(Section regSection) {
        Bundle args = new Bundle();

        DialogRegister fragment = new DialogRegister();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires once view is created. First calls super to set up shared dialog elements, then proceeds
     * to set up elements specific to this dialog.
     *
     * @param view               is of type {@link View} and is the view returned from {@link
     *                           #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's saved state
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setPosBtnVisibility(View.VISIBLE);
        setPosBtnText(R.string.dialog_pos_btn_default);
    }

    /**
     * Retrieves all content layout resource IDs used in this dialog.
     *
     * @return is of primitive array type int.
     */
    @Override
    protected int[] getContentLayouts() {return new int[0];}

    /**
     * Retrieves this dialog's width layout param, set in Super's {@link #onStart()}
     *
     * @return is of primitive type int and is this dialog's width layout param.
     */
    @Override
    protected int getWidthLayoutParams() {return 0;}

    /**
     * Retrieves this dialog's height layout param, set in Super's {@link #onStart()}
     *
     * @return is of primitive type int and is this dialog's height layout param.
     */
    @Override
    protected int getHeightLayoutParams() {return 0;}

    /**
     * Handles all clicks for this dialog. Should handle positive and negative button clicks.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button, which is either
     *              {@link R.id#dialog_neg_btn} or {@link R.id#dialog_pos_btn}
     */
    @Override
    protected void onBtnClick(int btnId) {}

    /**
     * Retrieves view to bind Soft Input to in Super. This Dialog does not use this method so it
     * returns null.
     *
     * @return is of type {@link View} and is nullable.
     */
    @Nullable
    @Override
    protected View getViewForSoftInput() {return null;}
}
