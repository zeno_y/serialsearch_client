package fragment.dialog;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.echo_usa.echotech.R;

import java.util.ArrayList;
import java.util.List;

import model.SectionValue;

/**
 * Created by Zeno Yuki on 4/25/18.
 *
 * Extends {@link DialogWrapper} to maintain uniformity both in the code and visually.
 *
 * Sets up a dialog to display details regarding a certain result section.
 *
 * This dialog receives in a number of {@link SectionValue} through {@link #newInstance(List)} and
 * displays as many TextViews as the number of items within the passed list.
 */
public class DialogDetails extends DialogWrapper {
    //Key for count of list passed through in newInstance()
    private static final String KEY_COUNT = "count";
    //Key for the list of values passed through in newInstance()
    private static final String KEY_VALUES = "values";

    /**
     * Generates a new instance of this class. Used so that Framework is forced to "pass parameters"
     * using the newInstance() method instead of a bare constructor.
     *
     * Stores the param as a {@link Bundle} for later access.
     *
     * @param values is non-null and is of type {@link List<SectionValue>}.
     * @return is of this type and is the newly instantiated dialog object.
     */
    public static DialogDetails newInstance(@NonNull List<SectionValue> values) {
        Bundle args = new Bundle();
        DialogDetails fragment = new DialogDetails();

        args.putInt(KEY_COUNT, values.size());
        args.putParcelableArrayList(KEY_VALUES, (ArrayList<? extends Parcelable>)values);

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires once view is created. First calls super to set up shared dialog elements, then proceeds
     * to set up elements specific to this dialog.
     *
     * @param view               is of type {@link View} and is the view returned from {@link
     *                           #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's saved state
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setPosBtnVisibility(View.GONE);
        if(getArguments() == null) return; //Do nothing if arguments is null

        List<SectionValue> valuesList = getArguments().getParcelableArrayList(KEY_VALUES);
        if(valuesList == null) return;

        //Cycle through all available views except for ButtonBarLayout (last child)
        LinearLayout layout = (LinearLayout)view.findViewById(R.id.dialog_root);
        final int textViewCount = layout.getChildCount();

        for(int i = 0; i < textViewCount - 1; i++) { //Accounts for ButtonBarLayout (last child)
            try {
                TextView tv = (TextView)layout.getChildAt(i);
                tv.setText(valuesList.get(i).getValue());
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Retrieves all content layout resource IDs used in this dialog. Returned array count will
     * equal the number of items in the list passed through in {@link #newInstance(List)}.
     *
     * @return is of primitive array type int.
     */
    @Override
    protected int[] getContentLayouts() {
        final int count = getArguments() != null ? getArguments().getInt(KEY_COUNT) : 0;
        int[] layouts = new int[count];

        for(int i = 0; i < count; i++) layouts[i] = R.layout.item_dialog_detail;
        return layouts;
    }

    /**
     * Retrieves this dialog's width layout param, set in Super's {@link #onStart()}
     *
     * @return is of primitive type int and is this dialog's width layout param.
     */
    @Override
    protected int getWidthLayoutParams() {return LinearLayout.LayoutParams.MATCH_PARENT;}

    /**
     * Retrieves this dialog's height layout param, set in Super's {@link #onStart()}
     *
     * @return is of primitive type int and is this dialog's height layout param.
     */
    @Override
    protected int getHeightLayoutParams() {return LinearLayout.LayoutParams.WRAP_CONTENT;}

    /**
     * Handles all clicks for this dialog. Should handle only a negative button click.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button, which is either
     *              {@link R.id#dialog_neg_btn} or {@link R.id#dialog_pos_btn}
     */
    @Override
    protected void onBtnClick(int btnId) {
        if(btnId == R.id.dialog_neg_btn) dismiss();
    }

    /**
     * Retrieves view to bind Soft Input to in Super. This Dialog does not use this method so it
     * returns null.
     *
     * @return is of type {@link View} and is nullable.
     */
    @Nullable
    @Override
    protected View getViewForSoftInput() {return null;}
}
