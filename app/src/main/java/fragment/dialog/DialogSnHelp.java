package fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.echo_usa.echotech.R;

/**
 * Created by Zeno Yuki on 7/18/18.
 *
 * Extends {@link DialogWrapper} to maintain uniformity both in the code and visually.
 *
 * Sets up dialog to display information regarding serial number structure, most likely used to
 * inform the person using this application that the searched serial number is incorrect.
 */
public class DialogSnHelp extends DialogWrapper {
    /**
     * Generates a new instance of this class. Used so that Framework is forced to "pass parameters"
     * using the newInstance() method instead of a bare constructor.
     *
     * This particular newInstance() method has no params.
     *
     * @return is of this type and is the newly instantiated dialog object.
     */
    public static DialogSnHelp newInstance() {
        Bundle args = new Bundle();
        DialogSnHelp fragment = new DialogSnHelp();

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires once view is created. First calls super to set up shared dialog elements, then proceeds
     * to set up elements specific to this dialog.
     *
     * @param view               is of type {@link View} and is the view returned from {@link
     *                           #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's saved state
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setPosBtnVisibility(View.GONE);
    }

    /**
     * Retrieves all content layout resource IDs used in this dialog.
     *
     * @return is of primitive array type int.
     */
    @Override
    protected int[] getContentLayouts() {return new int[]{R.layout.item_dialog_sn_structure};}

    /**
     * Retrieves this dialog's width layout param, set in Super's {@link #onStart()}
     *
     * @return is of primitive type int and is this dialog's width layout param.
     */
    @Override
    protected int getWidthLayoutParams() {return LinearLayout.LayoutParams.MATCH_PARENT;}

    /**
     * Retrieves this dialog's height layout param, set in Super's {@link #onStart()}
     *
     * @return is of primitive type int and is this dialog's height layout param.
     */
    @Override
    protected int getHeightLayoutParams() {return LinearLayout.LayoutParams.WRAP_CONTENT;}

    /**
     * Handles all clicks for this dialog. Should handle only a negative button click.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button, which is either
     *              {@link R.id#dialog_neg_btn} or {@link R.id#dialog_pos_btn}
     */
    @Override
    protected void onBtnClick(int btnId) {
        if(btnId == R.id.dialog_neg_btn) dismiss();
    }

    /**
     * Retrieves view to bind Soft Input to in Super. This Dialog does not use this method so it
     * returns null.
     *
     * @return is of type {@link View} and is nullable.
     */
    @Nullable
    @Override
    protected View getViewForSoftInput() {return null;}
}
