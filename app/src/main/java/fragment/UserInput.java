package fragment;

/**
 * Created by Zeno Yuki on 7/19/18.
 *
 * Interface used to notify fragments when to "re-authorize" user input. This is used to prevent
 * multiple UI clicks, and subsequently multiple calls to a Volley class or a Fragment Transaction.
 */
public interface UserInput {
    /**
     * Fires to request a reset of the variable keeping track of user input.
     */
    void reset();
}
