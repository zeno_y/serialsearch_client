package fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.echo_usa.echotech.R;
import com.echo_usa.echotech.SerialSearchActivity;

import java.util.ArrayList;
import java.util.List;

import adapter.SectionAdapter;
import model.Section;
import model.SectionValue;

/**
 * Created by Zeno Yuki on 3/22/18.
 *
 * Sets up a fragment to display and handle search results.
 *
 * Holds one list of items, some being clickable views that direct the user to another activity. See
 * {@link SectionAdapter} for more details.
 *
 * The RecyclerView here should never be null due to how {@link SectionAdapter} behaves.
 * Therefore the non-custom recycler is used.
 */
public class FragmentResult extends Fragment implements View.OnClickListener, UserInput {
    //Key for the list of sections passed through the newInstance() method.
    private static final String KEY_RESULT_LIST = "results_list";
    private SectionAdapter mAdapter;

    //Keeps track of whether or not a user input has been received. This is done to prevent
    // multiple consecutive clicks to an input, preventing consecutive Volley or Fragment
    // Transaction calls.
    private boolean mUserInputReceived = false;

    /**
     * Generates a new instance of this class. Used so that Framework is forced to "pass parameters"
     * using the newInstance() method instead of a bare constructor.
     *
     * Stores the param as a {@link Bundle} for later access.
     *
     * @param sectionList is of type {@link List<Section>}
     * @return is of this type and is the newly instantiated dialog object.
     */
    public static FragmentResult newInstance(List<Section> sectionList) {
        Bundle args = new Bundle();

        FragmentResult fragment = new FragmentResult();
        args.putParcelableArrayList(KEY_RESULT_LIST, (ArrayList<? extends Parcelable>) sectionList);

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires when fragment is called to "resume". Resets input-received variable so that fragment
     * will restart accepting user input.
     */
    @Override
    public void onResume() {
        super.onResume();
        setUserInputReceived(false);
    }

    /**
     * Fires to inflate the view for this fragment.
     *
     * @param inflater           is of type {@link LayoutInflater} and is the inflater used to
     *                           inflate the view.
     * @param container          is of type {@link ViewGroup} and is the parent view/viewgroup of
     *                           this fragment.
     * @param savedInstanceState is of type {@link Bundle} and is the previously saved state, if
     *                           any.
     * @return is of type {@link View} and is the inflated view to be used for this fragment.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_result, container, false);
    }

    /**
     * Fires once view is created. Sets up view/UI elements here.
     *
     * @param view               is of type {@link View} and is the view inflated in {@link
     *                           #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is nullable and is of type {@link Bundle}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recycler = (RecyclerView)view.findViewById(R.id.result_recycler);
        recycler.setHasFixedSize(true); //Currently all item_result layouts have height of @dimen/result_item_size
        recycler.setLayoutManager(new LinearLayoutManager(
                getContext(), LinearLayoutManager.VERTICAL, false
        ));

        if(getArguments() == null || getArguments().isEmpty()) return;
        List<Section> list = getArguments().getParcelableArrayList(KEY_RESULT_LIST);

        mAdapter = new SectionAdapter(list, FragmentResult.this);
        recycler.setAdapter(mAdapter);

        //Sets empty view for RecyclerView
//        TextView emptyView = view.findViewById(R.id.list_empty_view);
//        emptyView.setText(R.string.text_no_results);
//        recycler.setEmptyView(emptyView);
    }

    /**
     * Implements from {@link UserInput} Resets variable responsible for tracking user input.
     */
    @Override
    public void reset() {setUserInputReceived(false);}

    /**
     * Implements from {@link View.OnClickListener} Handles individual item clicks in the history
     * item list, the Home As Up button overlay ("overlay" because the drawer hides the original
     * button) and the history clear button.
     *
     * @param v is of type {@link View} and is the view that was clicked.
     */
    //TODO: still incomplete. SectionValue needs a type variable to distinguish PDF and URL
    @Override
    public void onClick(View v) {
        try {
            if(v.getId() == R.id.item_result_link) {
                //Prevents consecutive clicks
                if(getUserInputReceived()) return;
                else setUserInputReceived(true);

                SectionValue sv = v.getTag() != null ? (SectionValue) v.getTag() : null;
                if(sv != null) Log.i(getClass().getSimpleName(), "item clicked: " + sv.getValue());

                ((SerialSearchActivity) getActivity()).startWebViewActivity("mock_URL");

                return;
            }

            Section s = v.getTag() != null ? (Section) v.getTag() : null;
            switch(v.getId()) {
                case R.id.button_left:
                    ((SerialSearchActivity) getActivity()).showDetailsDialog(s);
                    if(s != null)
                        Log.i(getClass().getSimpleName(), "left btn clicked: " + s.getSerialNum());
                    break;
                case R.id.button_right:
                    ((SerialSearchActivity) getActivity()).showRegisterDialog(s);
                    if(s != null)
                        Log.i(getClass().getSimpleName(), "right btn clicked: " + s.getSerialNum());
                    break;
            }
        } catch(ClassCastException e) {
            e.printStackTrace();
        }
    }

    //Setters & Getters
    private void setUserInputReceived(boolean received) {mUserInputReceived = received;}

    private boolean getUserInputReceived() {return mUserInputReceived;}
}
