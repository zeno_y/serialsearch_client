package data;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Zeno Yuki on 3/28/18.
 *
 * SQLite table that holds search history entries.
 */
class TableHistory {
    //String constants of all columns for this table.
    static final String TABLE_NAME = "search_history";
    static final String KEY_ID = "_id";
    static final String MODEL_NAME = "model_name";
    static final String SERIAL_NUM = "serial_number";
    static final String SEARCH_DATE = "search_date";

    //String array of all columns for this table.
    static final String[] ALL_COLUMNS = new String[]{
            KEY_ID,
            MODEL_NAME,
            SERIAL_NUM,
            SEARCH_DATE
    };

    //String of SQL code used for creating search history table.
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            MODEL_NAME + " TEXT NOT NULL, " +
            SERIAL_NUM + " TEXT NOT NULL, " +
            SEARCH_DATE + " INTEGER NOT NULL);";

    /**
     * Fires when table is created.
     *
     * @param db is of type {@link SQLiteDatabase} and is the db to create the table in.
     */
    static void onCreate(SQLiteDatabase db) {db.execSQL(CREATE_TABLE);}

    /**
     * Fires when table is upgraded.
     *
     * @param db is of type {@link SQLiteDatabase} and is the db to upgrade the table in.
     */
    static void onUpgrade(SQLiteDatabase db) {
        //TODO: what to do when database is upgraded
    }
}
