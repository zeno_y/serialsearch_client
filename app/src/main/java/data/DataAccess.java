package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import model.HistoryItem;
import model.SectionType;

/**
 * Created by Zeno Yuki on 3/28/18.
 *
 * Responsible for access data in SQLite database.
 */
public class DataAccess {
    private static final Character SPACE = ' '; //Explicitly define a space character for ease of readability
    private static final String ASCENDING = SPACE + "ASC"; //Used to sort entries in Cursors
    private static final String DESCENDING = SPACE + "DESC"; //Used to sort entries in Cursors

    private SQLiteDatabase mDb;
    private DatabaseHelper mHelper;

    /**
     * Constructor. Retrieves instance of {@link DatabaseHelper} and stores reference.
     *
     * @param context is of type {@link Context} and is the context used when retrieving instance
     *                for {@link DatabaseHelper}
     */
    public DataAccess(Context context) {mHelper = DatabaseHelper.getInstance(context);}

    /**
     * Retrieves and stores an instance of a writable SQLite database.
     */
    public void open() {mDb = mHelper.getWritableDatabase();}

    /**
     * Closes database instance.
     */
    public void close() {mDb.close();}

    /**
     * Getter method for stored instance of {@link SQLiteDatabase}. Must call {@link #open()} before
     * using.
     *
     * @return is of type {@link SQLiteDatabase} and is the instance of an open SQLite database.
     */
    private SQLiteDatabase getDb() {return mDb;}

    /**
     * Retrieves search history list.
     *
     * @return is of type {@link List} and holds items of type {@link HistoryItem}
     */
    @NonNull
    public List<HistoryItem> getSearchHistoryList() {
        List<HistoryItem> list = new ArrayList<>();
        Cursor hCursor = getHistoryCursor();

        //Checks to see if there's at least one entry visible to the cursor
        if(hCursor.moveToFirst()) {
            //Iterates through all entries
            do {
                HistoryItem item = new HistoryItem(
                        getLong(hCursor, TableHistory.KEY_ID), //Retrieves ID from cursor
                        getString(hCursor, TableHistory.MODEL_NAME), //Retrieves model name from cursor
                        getString(hCursor, TableHistory.SERIAL_NUM), //Retrieves serial number from cursor
                        getLong(hCursor, TableHistory.SEARCH_DATE) //Retrieves seach date from cursor
                );

                list.add(item);
            } while(hCursor.moveToNext());
        }

        //Closes cursor to prevent memory leak
        hCursor.close();
        return list;
    }

    /**
     * Retrieves a section type by ID from the local database.
     *
     * @param id is of primitive type long and is the ID of the Section Type to retrieve.
     * @return is non-null and of type {@link SectionType}
     */
    @NonNull
    public SectionType getSectionType(long id) {
        SectionType sectionType = new SectionType();
        Cursor sCursor = getSectionTypeCursor(id);

        if(sCursor.moveToFirst()) {
            sectionType.setId(getLong(sCursor, TableSectionType.KEY_ID));
            sectionType.setName(getString(sCursor, TableSectionType.SECTION_NAME));
            sectionType.setHasButtons(getInt(sCursor, TableSectionType.HAS_BUTTONS) == SectionType.TRUE);
            sectionType.setOrder(getInt(sCursor, TableSectionType.ORDER));
        }

        return sectionType;
    }

    /**
     * Stores the search as a new entry in the database.
     *
     * @param model  is of type {@link String} and is the name of the model searched.
     * @param serial is of type {@link String} and is the serial number searched.
     * @param date   is of primitive type long and is the {@link System#currentTimeMillis()} at the
     *               exact time of search.
     * @return is of primitive type long and is the database ID of the newly created search entry.
     */
    public long storeSearch(String model, String serial, long date) {
        ContentValues values = new ContentValues();

        values.put(TableHistory.MODEL_NAME, model);
        values.put(TableHistory.SERIAL_NUM, serial);
        values.put(TableHistory.SEARCH_DATE, date);

        return getDb().insert(TableHistory.TABLE_NAME, null, values);
    }

    /**
     * Stores section type entry to local, retrieved from remote.
     *
     * @param id         is of primitive type long and is the ID of the type, generated by remote.
     * @param name       is of type {@link String} and is the name of the type.
     * @param hasButtons is of primitive type boolean and is true if type should display buttons.
     * @param order      is of primitive type int and is the order to display the sections.
     * @return is of primitive type long and is the row ID of the newly inserted entry or -1 if an
     * error has occurred.
     * @throws SQLException is thrown when database fails to insert a new row.
     */
    public long storeSectionType(long id, String name, boolean hasButtons, int order) throws
            SQLException {
        ContentValues values = new ContentValues();

        values.put(TableSectionType.KEY_ID, id);
        values.put(TableSectionType.SECTION_NAME, name);
        values.put(TableSectionType.HAS_BUTTONS, hasButtons ? SectionType.TRUE : SectionType.FALSE);
        values.put(TableSectionType.ORDER, order);

        return getDb().insertOrThrow(TableSectionType.TABLE_NAME, null, values);
    }

    /**
     * Clears all entries in the search history table.
     *
     * @return is of primitive type int and is the number of entries that were deleted.
     */
    public int clearHistory() {return getDb().delete(TableHistory.TABLE_NAME, "1", null);}

    /**
     * Updates search history entry with new search date.
     *
     * @param id      is of primitive type long and is the ID of the entry to update.
     * @param newDate is of primitive type long and is the new search date.
     * @return is of primitive type int and is the number of rows affected. Should be 0 or 1 in this
     * case.
     */
    public int updateSearchHistory(long id, long newDate) {
        ContentValues values = new ContentValues();
        values.put(TableHistory.SEARCH_DATE, newDate);

        return getDb().update(
                //No need for a literal WHERE in the whereClause param
                TableHistory.TABLE_NAME, values, TableHistory.KEY_ID + " =?",
                new String[]{String.valueOf(id)}
        );
    }

    /**
     * Retrieves cursor that navigates through all entries in the search history table.
     *
     * @return is of type {@link Cursor} and is the cursor to use to navigate through database
     * entries.
     */
    private Cursor getHistoryCursor() {
        return getDb().query(TableHistory.TABLE_NAME,
                TableHistory.ALL_COLUMNS,
                null, null, null, null,
                TableHistory.SEARCH_DATE + DESCENDING
        );
    }

    /**
     * Retrieves cursor that navigates through all entries in the section type table.
     *
     * @return is of type {@link Cursor} and is the cursor to use to navigate through database
     * entries.
     */
    private Cursor getSectionTypeCursor() {
        return getDb().query(TableSectionType.TABLE_NAME,
                TableSectionType.ALL_COLUMNS,
                null, null, null, null,
                TableSectionType.KEY_ID + ASCENDING
        );
    }

    /**
     * Retrieves cursor that navigates through one entry in the section type table.
     *
     * @param id is of primitive type long and is the ID of the entry to navigate through.
     * @return is of type {@link Cursor} and is the cursor to use to navigate through the database
     * entry.
     */
    private Cursor getSectionTypeCursor(long id) {
        return getDb().query(TableSectionType.TABLE_NAME,
                TableSectionType.ALL_COLUMNS,
                TableSectionType.KEY_ID + " =?", new String[]{String.valueOf(id)},
                null, null,
                TableSectionType.KEY_ID + ASCENDING
        );
    }

    /**
     * Shadow method for {@link Cursor#getString(int)} Used for making code easier to read.
     *
     * @param c          is of class {@link Cursor}
     * @param columnName is of class {@link String}. Caution: Method assumes {@param columnName} is
     *                   a valid column within the SQLite database table that {@param c} resides
     *                   in.
     * @return The value of that column as a String value
     * @see Cursor#getString(int)
     */
    private static String getString(@NonNull Cursor c, @NonNull String columnName) {
        return c.getString(c.getColumnIndex(columnName));
    }

    /**
     * Shadow method for {@link Cursor#getInt(int)} Used for making code easier to read.
     *
     * @param c          is of class {@link Cursor}
     * @param columnName is of class {@link String}. Caution: Method assumes {@param columnName} is
     *                   a valid column within the SQLite database table that {@param c} resides
     *                   in.
     * @return The value of that column as an int value
     * @see Cursor#getInt(int)
     */
    private static int getInt(@NonNull Cursor c, @NonNull String columnName) {
        return c.getInt(c.getColumnIndex(columnName));
    }

    /**
     * Shadow method for {@link Cursor#getLong(int)} Used for making code easier to read.
     *
     * @param c          is of class {@link Cursor}
     * @param columnName is of class {@link String}. Caution: Method assumes {@param columnName} is
     *                   a valid column within the SQLite database table that {@param c} resides
     *                   in.
     * @return The value of that column as a long value
     * @see Cursor#getLong(int)
     */
    private static long getLong(@NonNull Cursor c, @NonNull String columnName) {
        return c.getLong(c.getColumnIndex(columnName));
    }
}
