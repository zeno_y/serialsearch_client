package data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Zeno Yuki on 3/28/18.
 *
 * Database helper class that creates and upgrades all tables when necessary.
 */
class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper sInstance; //Reference to single instance of this class.

    private static final String DB_NAME = "database"; //Database name
    private static final int DB_VER = 1; //Database version

    /**
     * Constructor. Is private and calls super {@link SQLiteOpenHelper#SQLiteOpenHelper(Context,
     * String, SQLiteDatabase.CursorFactory, int)}
     *
     * @param c is of type {@link Context} and is the context of the method that calls this.
     */
    private DatabaseHelper(Context c) {super(c, DB_NAME, null, DB_VER);}

    /**
     * Retrieves instance of this class. Forces this class to act as a singleton by preventing new
     * instances from being created.
     *
     * @param c is of type {@link Context} and is the context of the method that calls this.
     * @return is of type {@link DatabaseHelper} and is the instance of this class.
     */
    static synchronized DatabaseHelper getInstance(Context c) {
        if(sInstance == null) sInstance = new DatabaseHelper(c);
        return sInstance;
    }

    /**
     * Fires when database tables are being created.
     *
     * @param db is of type {@link SQLiteDatabase} and is the database the tables are created in.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        TableHistory.onCreate(db);
        TableSectionType.onCreate(db);
    }

    /**
     * Fires when database tables need upgraded.
     *
     * @param db         is of type {@link SQLiteDatabase} and is the database the tables will
     *                   upgrade in.
     * @param oldVersion is of primitive type int and is the old database version
     * @param newVersion is of primitive type int and is the new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion > oldVersion) {
            //TODO: do something when database needs upgrading
        }
    }
}
