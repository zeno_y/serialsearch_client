package data;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Zeno Yuki on 7/16/18.
 *
 * SQLite table that holds section type entries.
 */
//TODO: what happens when server has different order than client?
public class TableSectionType {
    //String constants of all columns within this table
    static final String TABLE_NAME = "section_types";
    static final String KEY_ID = "_id";
    static final String SECTION_NAME = "name";
    static final String HAS_BUTTONS = "has_buttons";
    static final String ORDER = "order_num";

    //String array containing all columns within this table
    static final String[] ALL_COLUMNS = new String[]{
            KEY_ID,
            SECTION_NAME,
            HAS_BUTTONS,
            ORDER
    };

    //String of SQL code used for creating search history table.
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            KEY_ID + " INTEGER PRIMARY KEY, " +
            SECTION_NAME + " TEXT NOT NULL, " +
            HAS_BUTTONS + " INTEGER NOT NULL DEFAULT 0, " +
            ORDER + " INTEGER NOT NULL);";

    /**
     * Fires when table is created.
     *
     * @param db is of type {@link SQLiteDatabase} and is the db to create the table in.
     */
    static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    /**
     * Fires when table is upgraded.
     *
     * @param db is of type {@link SQLiteDatabase} and is the db to upgrade the table in.
     */
    static void onUpgrade(SQLiteDatabase db) {
        //TODO: do something when upgrading database.
    }
}
